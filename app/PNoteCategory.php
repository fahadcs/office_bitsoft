<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PNoteCategory extends Model
{
    

    public function notes()
    {
        return $this->hasMany(PNoteItem::class);
    }

    public function getNotesById($project_id, $role_id){
        if($role_id == 2){
            return $this->notes()->where('project_id', '=', $project_id)->where('show_to_member', true)->get();
        }
        return $this->notes()->where('project_id', '=', $project_id)->get();

    }
}
