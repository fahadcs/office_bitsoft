<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Attendance extends Model
{
    protected $dates = ['clock_in_time', 'clock_out_time'];
    protected $appends = ['clock_in_date'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id')->withoutGlobalScopes(['active']);
    }

    public function getClockInDateAttribute()
    {
        return $this->clock_in_time->toDateString();
    }

    public static function attendanceByDate($date) {
        return User::leftJoin(
                'attendances', function ($join) use ($date) {
                    $join->on('users.id', '=', 'attendances.user_id')
                        ->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $date);
                }
            )
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->leftJoin('employee_details', 'employee_details.user_id', '=', 'users.id')
            ->where('roles.name', '<>', 'client')
            ->where('users.is_remote_check', false)
            ->select(
                'users.id',
                'users.name',
                'attendances.clock_in_ip',
                'attendances.clock_in_time',
                'attendances.clock_out_time',
                'attendances.late',
                'attendances.half_day',
                'attendances.working_from',
                'attendances.work_summary',
                'users.image',
                'employee_details.job_title',
                'attendances.id as attendance_id'
            )
            ->groupBy('users.id')
            ->orderBy('users.name', 'asc');
    }

    public static function userAttendanceByDate($startDate, $endDate, $userId) {
        if($userId == 0){
            return Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->orderBy('attendances.id', 'desc')
            ->select('attendances.*', 'users.*', 'attendances.id as aId')
            ->get();
        }
        else{
            return Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->where('attendances.user_id', '=', $userId)
            ->orderBy('attendances.id', 'desc')
            ->select('attendances.*', 'users.*', 'attendances.id as aId')
            ->get();
        }
       
    }

    public static function countDaysPresentByUser($startDate, $endDate, $userId){
        if($userId == 0){
            return Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
           
            ->count();
        }
        else{
            return Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->where('user_id', $userId)
            ->count();
        }
        
    
    }

    public static function countTotalHours($startDate, $endDate, $userId, $global){
        $attendance = Attendance::where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
        ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
        ->where('user_id', $userId)->get();
        $time = 0;
       
        foreach($attendance as $data){
            if(isset($data->clock_in_time) && isset($data->clock_out_time) ){
                $time += $data->clock_in_time->diffInMinutes($data->clock_out_time);
            }
            

        }
        $hours = floor( $time / 60 ); 
        $minute = $time % 60;

        $total_time = $hours.':'.$minute;
        return $time;
    }
    public static function countTotalHoursByUser($startDate, $endDate, $userId, $global){
        if($userId == 0){
            $attendance = Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->where('users.is_remote_check',false)
            ->get();
            $time = 0;
            foreach($attendance as $data){
                if(isset($data->clock_in_time) && isset($data->clock_out_time) ){
                    $time += $data->clock_in_time->diffInMinutes($data->clock_out_time);
                }
                

            }
            $hours = floor( $time / 60 ); 
            $minute = $time % 60;

            $total_time = $hours.':'.$minute;
            return $time;
        }
        else{
            $attendance = Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->where('users.is_remote_check',false)
            ->where('user_id', $userId)->get();
            
            $time = 0;
            foreach($attendance as $data){
                if(isset($data->clock_in_time) && isset($data->clock_out_time) ){
                    $time += $data->clock_in_time->diffInMinutes($data->clock_out_time);
                }
                

            }
            $hours = floor( $time / 60 ); 
            $minute = $time % 60;

            $total_time = $hours.':'.$minute;
            return $time;
        }
        
    }

    public static function countDaysLateByUser($startDate, $endDate, $userId){
        if($userId == 0){
            return Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->where('late', 'yes')
            ->where('users.is_remote_check',false)
            ->count();
        }
        else{
            return Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->where('user_id', $userId)
            ->where('late', 'yes')
            ->where('users.is_remote_check',false)
            ->count();
        }
        
    }

    public static function countHalfDaysByUser($startDate, $endDate, $userId){
        if($userId == 0){
            return Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->where('half_day', 'yes')
            ->where('users.is_remote_check',false)
            ->count();
        }
        else{
            return Attendance::join('users', 'users.id', '=', 'attendances.user_id')
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $startDate)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $endDate)
            ->where('user_id', $userId)
            ->where('half_day', 'yes')
            ->where('users.is_remote_check',false)
            ->count();
        }
        
    }

    protected $guarded = ['id'];
}
