<?php

namespace App\Http\Controllers;
use App\Account;
use Auth;
use App\VoucherDetail;
use App\voucher_master;
use App\Helper\Reply;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Validator;
use Session;
use DB;
use Illuminate\Http\Request;


class VoucherController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'Vouchers';
        $this->pageIcon = 'icon-clock';
    }


    public function index()
    {
        $user = auth()->user();
        $id = Session::get("voucher_master_id");
        $this->existing = null;
        $this->detailexist = null;
        $this->masterID = null;

        if(isset($id)){
                $this->existing = voucher_master::find($id);
                if ($this->existing) {
                    $this->masterID = $id;
                    $this->detailexist = DB::table('voucher_details')
                        ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->select('voucher_details.id as vid', 'voucher_details.narration as narration', 'voucher_details.debit as debit', 'voucher_details.credit as credit', 'accounts.account_name as account_name', 'voucher_details.account_id as accountid')
                        ->where(['voucher_details.voucher_master_id' => $id])->get();
                    //$detailexist = VoucherDetail::where('voucher_master_id',$id)->get();
                }
               
        }
        

       
            
            $this->user = auth()->user();
            $this->accountname = Account::select('id', 'account_name', 'user_id')->where('user_id',$this->user->id)->where('is_parent', '=', false)->get();
    
            return view('accounts.vouchers.index', $this->data);
       



       
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'voucher_type' => 'required',
            'account_name' => 'required',
            'account_id' => 'required',
            'narration' => 'required',
            'debit' => 'required',
            'credit' => 'required'
        ]);

        if ($validator->passes()) {
            if (!Session::get('voucher_master_id')) {
                $voucher_master = new voucher_master;
                $voucher_master->date = date("Y-m-d", strtotime($request->date));
                $voucher_master->voucher_type = $request->input('voucher_type');
                $voucher_master->save();

                Session::put("voucher_master_id", $voucher_master->id);
            } else {
                $voucher_master = voucher_master::find(Session::get('voucher_master_id'));
                $voucher_master->date = date("Y-m-d", strtotime($request->date));
                $voucher_master->voucher_type = $request->input('voucher_type');
                $voucher_master->save();
            }
            //echo "<pre>"; print_r($request->all()); exit;
            //  echo '<pre>'; print_r($request->all()); exit;

            //$voucher_master->id;
            //echo($voucher_master->id);

            if ($request->input('voucher_detail_id')) {
                $voucher_detail = VoucherDetail::find($request->voucher_detail_id);
                $voucher_detail->account_id = $request->input('account_id');
                $voucher_detail->narration = $request->input('narration');
                $voucher_detail->debit = $request->input('debit');
                $voucher_detail->credit = $request->input('credit');
                $voucher_detail->voucher_master_id = Session::get("voucher_master_id");
                $voucher_detail->save();
            } else {
                $voucher_detail = new VoucherDetail;
                $voucher_detail->account_id = $request->input('account_id');
                $voucher_detail->narration = $request->input('narration');
                $voucher_detail->debit = $request->input('debit');
                $voucher_detail->credit = $request->input('credit');
                $voucher_detail->voucher_master_id = Session::get("voucher_master_id");
                $voucher_detail->save();
            }

            return response()->json(['success' => true, 'voucher_detail' => $voucher_detail]);
        }

        return response()->json(['success' => false, 'errors' => $validator->errors()->all()]);

        // $this->validate($request->all(),[
        //     'date'=>'required',
        //     'voucher_type'=>'required',
        //     'account_name'=>'required',
        //     'account_id'=>'required',
        //     'narration'=>'required',
        //     'debit'=>'required',
        //     'credit'=>'required'
        // ]);
        // return response()->json(['success'=>'Added new records.']);
    }


    public function saveform(){
        if(Session::get('voucher_master_id')) {
            session()->forget('voucher_master_id');
            //unset($_SESSION['voucher_master_id']);
            //return back();
            //window.location.reload();
            return response()->json(['success' => true]);
            //return response()->json(['success'=>true]);
        }
        return response()->json(['success' => false]);
    }

    public function data(){
        $user = auth()->user();

        $record = DB::table('voucher_masters')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type')
            ->join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->where(['accounts.user_id' => $user->id])->distinct()->get();


        return DataTables::of($record)
            ->addColumn('debit_sum', function ($row) {
                $voucher_details = VoucherDetail::where('voucher_master_id', $row->id)->get();
                $debit_sum = 0;
                foreach ($voucher_details as $key => $value) {
                    $debit_sum += $value->debit;
                }
                return $debit_sum;
            })
            ->addColumn('credit_sum', function ($row) {
                $voucher_details = VoucherDetail::where('voucher_master_id', $row->id)->get();
                $credit_sum = 0;
                foreach ($voucher_details as $key => $value) {
                    $credit_sum += $value->credit;
                }
                return $credit_sum;
            })
            ->addColumn('action', function ($row) {

                    return '<a href="' .route('accounts_manager.vouchers.edit', ['voucher' => $row->id]) . '" class="btn btn-info btn-circle"
                        data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
  
                        <a onclick="return showDetails(' . $row->id . ')"  class="btn btn-success btn-circle"
                        data-toggle="tooltip" data-original-title="View Employee Details"><i class="fa fa-search" aria-hidden="true"></i></a>
  
                        <a href="javascript:;" class="btn btn-danger btn-circle delete_voucher"
                        data-toggle="tooltip" data-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';   
            
                    })
            ->rawColumns(['action', 'debit_sum', 'credit_sum'])
            ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    public function showDetail(Request $request){
        $record = DB::table('voucher_details')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_details.narration as narration', 'voucher_details.debit as debit', 'voucher_details.credit as credit', 'accounts.account_name as account_name')
            ->where(['voucher_details.voucher_master_id' => $request->voucher_master_id])->get();


        $voucher_details = VoucherDetail::where('voucher_master_id', $request->voucher_master_id)->get();
        $debit_sum = 0;
        foreach ($voucher_details as $key => $value) {
            $debit_sum += $value->debit;
        }

        $voucher_details = VoucherDetail::where('voucher_master_id', $request->voucher_master_id)->get();
        $credit_sum = 0;
        foreach ($voucher_details as $key => $value) {
            $credit_sum += $value->credit;
        }
        $vmaster = voucher_master::find($request->voucher_master_id);
        $vmaster->setAttribute('debit_sum', $debit_sum);
        $vmaster->setAttribute('credit_sum', $credit_sum);


        return response()->json(['success' => true, 'record' => $record, 'voucher_master' => $vmaster]);
    }

    public function delete(Request $request){
        $voucher_detail = VoucherDetail::find($request->voucher_detail_id);
        $voucher_detail->delete();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = auth()->user();
        $this->existing = voucher_master::find($id);
        $this->masterID = $id;
        $this->detailexist = DB::table('voucher_details')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_details.id as vid', 'voucher_details.narration as narration', 'voucher_details.debit as debit', 'voucher_details.credit as credit', 'accounts.account_name as account_name', 'voucher_details.account_id as accountid')
            ->where(['voucher_details.voucher_master_id' => $id])->get();
        //$detailexist = VoucherDetail::where('voucher_master_id',$id)->get();

        $this->accountname = Account::select('id', 'account_name', 'user_id')->where('user_id', $user->id)->where('is_parent', '=', false)->get();

        if ($this->existing) {
            Session::put("voucher_master_id", $id);
            return view('accounts.vouchers.index', $this->data);
        }
        return "Error";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing = voucher_master::find($id);

        if ($existing) {
            $existing->delete();
            VoucherDetail::where('voucher_master_id', $id)->delete();
            if(Session::get('voucher_master_id')) {
                session()->forget('voucher_master_id');
            }
            return Reply::redirect(route('accounts_manager.vouchers.index'), __('Vouchers Deleted'));
            
        } else {
            if(Session::get('voucher_master_id')) {
                session()->forget('voucher_master_id');
            }
            return Reply::redirect(route('accounts_manager.vouchers.index'), __('something went wrong'));
        }
    }
}
