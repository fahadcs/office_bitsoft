<?php

namespace App\Http\Controllers;
use App\Account;
use Auth;
use App\Helper\Reply;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Validator;
use App\VoucherDetail;
use App\voucher_master;
use Session;
use DB;
use Illuminate\Http\Request;

class LedgerController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'ledger';
        $this->pageIcon = 'icon-clock';
    }



    public function index(){
        $this->existing = null;
        $this->user = auth()->user();
        $this->accountname = Account::select('id', 'account_name', 'user_id')->where('user_id', $this->user->id)->where('is_parent', '=', false)->get();
        return view('accounts.ledger', $this->data);
    }

    public function data(Request $request){
        //echo '<pre>'; print_r($request->get('startDate')); exit;
        if(empty($request->account)&&empty($request->startDate)&&empty($request->endDate))
        {
            $user = auth()->user();
            $record = voucher_master::join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','accounts.account_name')
            ->where('accounts.user_id','=', $user->id)->orderBy('voucher_details.id', 'asc')->distinct()->get();
            $balance=0;
            foreach ($record as $key => $r) {
                $balance=($r->debit - $r->credit) + $balance;
                $r->setAttribute('balance', $balance);
            }
            return DataTables::of($record)
            ->make(true);
        }
        elseif (empty($request->startDate)&&empty($request->endDate)) {
            $user = auth()->user();
            $record = voucher_master::join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','accounts.account_name')
            ->where([
                ['accounts.user_id','=', $user->id],
                ['voucher_details.account_id','=', $request->account]
                ])
                ->orderBy('voucher_details.id', 'asc')
                ->distinct()->get();
            $balance=0;
            foreach ($record as $key => $r) {
                $balance=($r->debit - $r->credit) + $balance;
                $r->setAttribute('balance', $balance);
            }
            return DataTables::of($record)
            ->make(true);
        }
        elseif (empty($request->account)) {
            $user = auth()->user();
            $record = voucher_master::join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','accounts.account_name')
            ->where('accounts.user_id','=', $user->id)
            ->whereBetween('voucher_masters.date', [
                date("Y-m-d", strtotime($request->startDate)),
                date("Y-m-d", strtotime($request->endDate))])
            ->orderBy('voucher_details.id', 'asc')
            ->distinct()->get();
            $balance=0;
            foreach ($record as $key => $r) {
                $balance=($r->debit - $r->credit) + $balance;
                $r->setAttribute('balance', $balance);
            }
            return DataTables::of($record)
            ->make(true);
        }
        else {
            $user = auth()->user();
            $record = voucher_master::join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','accounts.account_name')
            ->where([
                ['accounts.user_id','=', $user->id],
                ['voucher_details.account_id','=', $request->account]
                ])
            ->whereBetween('voucher_masters.date', [
                date("Y-m-d", strtotime($request->startDate)),
                date("Y-m-d", strtotime($request->endDate))])
            ->orderBy('voucher_details.id', 'asc')
            ->distinct()->get();
            $balance=0;
            foreach ($record as $key => $r) {
                $balance=($r->debit - $r->credit) + $balance;
                $r->setAttribute('balance', $balance);
            }
            return DataTables::of($record)
            ->make(true);
        }

    }
}
