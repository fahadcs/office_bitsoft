<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ModuleSetting;
use App\Project;
use App\PNoteCategory;
use App\PNoteItem;
use App\Helper\Reply;
use Auth;
use App\Role;
use App\RoleUser;

class ManageNotesController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-layers';
        $this->pageTitle = __('app.menu.projects');
    }

    public function index($id){
        $user = Auth::user();
        //$user_role = $user->role;
        $user_role = RoleUser::where('user_id','=', $user->id)->first();
        $this->role_id = $user_role->role_id;
        $this->project = Project::findOrFail($id);
        $this->pNotesCategory = PNoteCategory::all();
        
         /* foreach($pNotesCategory as $data){
             
            echo '<pre>'; print_r($data->getNotesById($id)); 
         }  */
        return view('admin.projects.notes.index', $this->data);
    }

    public function saveCategory(Request $request){

        $category = new PNoteCategory();
        $category->name = $request->category_heading;
        if($request->category_description != ''){
            $category->discription = $request->category_description;
        }
        $category->save();
        $category = $category->toArray();
        $data = ['success' =>true, 'category' => $category];
        return $data;
    }

    public function saveNote(Request $request){
        $user = Auth::user();
        //$user_role = $user->role;
        $user_role = RoleUser::where('user_id','=', $user->id)->first();
        $this->role_id = $user_role->role_id;

        if(isset($request->id) && $request->id != ''){
            $note = PNoteItem::findOrFail($request->id);
            if($request->description != ''){
                $note->note = $request->description;
            }
           
            $note->p_note_category_id = $request->category;
            $note->project_id = $request->project_id;
            if($this->role_id == 2){
                $note->show_to_member = true;
            }
            else{
                if(isset($request->show_to_member) && $request->show_to_member = true){
                    $note->show_to_member = $request->show_to_member;
                }
                else{
                    $note->show_to_member = false;
                }
            }
            
            $note->save();
        }
        else{
            $note = new PNoteItem();
            if($request->description != ''){
                $note->note = $request->description;
            }
        
            $note->p_note_category_id = $request->category;
            $note->project_id = $request->project_id;
            if($this->role_id == 2){
                $note->show_to_member = true;
            }
            else{
                if(isset($request->show_to_member) && $request->show_to_member = true){
                    $note->show_to_member = $request->show_to_member;
                }
                else{
                    $note->show_to_member = false;
                }
            }
            $note->save();
        }
        
        $this->project_id = $request->project_id;
        $this->category = PNoteCategory::all();
        $view = view('admin.projects.notes.notes_card', $this->data)->render();

        $category_options = view('admin.projects.notes.category_options', $this->data)->render();
        $data = ['success' =>true, 'html' => $view, 'category_options' => $category_options];
        return $data;
    }

    public function editNote($id){
        $user = Auth::user();
        //$user_role = $user->role;
        $user_role = RoleUser::where('user_id','=', $user->id)->first();
        $this->role_id = $user_role->role_id;
        $this->note = PNoteItem::findOrFail($id);
        $this->pNotesCategory = PNoteCategory::all();
        $view = view('admin.projects.notes.edit', $this->data)->render();
        return Reply::dataOnly(['html' => $view]);
    }
   
}
