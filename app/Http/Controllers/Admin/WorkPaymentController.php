<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\User\UpdateEmployee;
use App\Leave;
use App\LeaveType;
use App\ModuleSetting;
use App\Notifications\NewUser;
use App\Project;
use App\ProjectTimeLog;
use App\Role;
use App\RoleUser;
use App\Task;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Edujugon\Laradoo\Odoo;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;
use Ripcord\Providers\Laravel\Ripcord;
use App\Payable;
use App\Attendance;
use DB;
use App\WorkPayment;
class WorkPaymentController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'WorkPayment';
        $this->pageIcon = 'icon-user';

        
    }


    public function index()
    {
        $this->startDate =  Carbon::now()->timezone($this->global->timezone)->startOfMonth()->subMonth();
        $this->endDate =  Carbon::now()->timezone($this->global->timezone)->endOfMonth()->subMonth()->addDay(1);
        $this->totalEmployees = count(User::allEmployees());
        $this->employees = User::allEmployees();
        return view('admin.work-payment.index', $this->data);
    }

    public function total_paid($startDate = null, $endDate = null, $userId = null){
        $total_paid = 0;
        if($userId   == '0'){
            $work_payments = WorkPayment::where(DB::raw('DATE(work_payments.start_date)'), '>=', $startDate)
            ->where(DB::raw('DATE(work_payments.end_date)'), '<=', $endDate)->get();

            foreach($work_payments as $data){
                $total_paid += $data->payment;
            }
        }
        else{
            $work_payments = WorkPayment::where(DB::raw('DATE(work_payments.start_date)'), '>=', $startDate)
            ->where(DB::raw('DATE(work_payments.end_date)'), '<=', $endDate)->where('user_id', $userId)->get();
            foreach($work_payments as $data){
                $total_paid += $data->payment;
            }
        }

        return Reply::dataOnly(['total_paid' => $total_paid]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->startDate =  Carbon::now()->timezone($this->global->timezone)->startOfMonth()->subMonth();
        $this->endDate =  Carbon::now()->timezone($this->global->timezone)->endOfMonth()->subMonth()->addDay(1);
        $this->employees = User::allEmployees();
        return view('admin.work-payment.create', $this->data);
    }


    public function refreshPayable($startDate = null, $endDate = null, $userId = null){
        $startDate = Carbon::createFromFormat('!Y-m-d', $startDate);
        $endDate = Carbon::createFromFormat('!Y-m-d', $endDate);
        $totalminutes = Attendance::countTotalHoursByUser($startDate, $endDate, $userId, $this->global);
        $current_payable = 0;
        $amount_till_now = 0;
        $payable=Payable::where('user_id','=',$userId)->whereDate('from_date', '<=', $endDate)->orderBy('from_date', 'DESC')->first();
        if($payable){
            $current_payable += $payable->amount;
        }
        else{
            $payable=Payable::where('user_id','=',$userId)->whereDate('from_date', '>=', $endDate)->orderBy('from_date', 'ASC')->first();
                if($payable){
                    $current_payable += $payable->amount;
                }
            
        }
        if($payable){
            $user = User::where('id','=',$userId)->first();
            $monthly_working_hours = $user->monthly_working_hours * 60;
            $hourly_rate = $current_payable / $monthly_working_hours;
       
            $amount_till_now = (int)($hourly_rate * $totalminutes);
        }

        $current_payable =(int)$current_payable;
        return Reply::dataOnly(['amount_till_now' => $amount_till_now]);
        
    }


    public function data(Request $request){

        $startDate = Carbon::createFromFormat('!Y-m-d', $request->get('start_date'));
        $endDate = Carbon::createFromFormat('!Y-m-d', $request->get('end_date'));

        $user_id = $request->get('userId');

       if($user_id != 0){
            $work_payments = WorkPayment::join('users', 'users.id', '=', 'work_payments.user_id')
            ->where(DB::raw('DATE(work_payments.start_date)'), '>=', $startDate)
            ->where(DB::raw('DATE(work_payments.end_date)'), '<=', $endDate)
            ->where('work_payments.user_id', $user_id)
             ->get(['work_payments.*', 'users.name']);
       }
       else{
            $work_payments = WorkPayment::join('users', 'users.id', '=', 'work_payments.user_id')
            ->where(DB::raw('DATE(work_payments.start_date)'), '>=', $startDate)
            ->where(DB::raw('DATE(work_payments.end_date)'), '<=', $endDate)
            ->get(['work_payments.*', 'users.name']);
       }
    

    return Datatables::of($work_payments)

        ->addColumn('action', function ($row) {
            return '<a href="' . route('admin.workPayment.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                <a href="javascript:;" class="btn btn-danger btn-circle delete_workPayment"
                  data-toggle="tooltip" data-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
        })
        ->editColumn(
            'date_range',
            function ($row) {
                
                    return '<p>'.date("m/d/Y", strtotime($row->start_date)).' - '.date("m/d/Y", strtotime($row->end_date)).'</p>';
                
                
            }
        )
        ->editColumn(
            'pay_date',
            function ($row) {
                
                    return '<p>'.date("m/d/Y", strtotime($row->pay_date)).'</p>';
                
                
            }
        )

        ->rawColumns(['date_range', 'action', 'pay_date'])
        ->make(true); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(isset($request->id)){

            $pay_date = Carbon::createFromFormat('m/d/Y', $request->pay_date)->format('Y-m-d');
            $work_payment = WorkPayment::findOrFail($request->id);
            $work_payment->user_id = $request->user_id;
            $work_payment->start_date = $request->startDate;
            $work_payment->end_date = $request->endDate;
            $work_payment->payment = $request->payment;
            $work_payment->pay_date = $pay_date;
            $work_payment->bonus = $request->bonus;
            $work_payment->balance = $request->balance;
            $work_payment->payable = $request->payable;
            $work_payment->save();
        }
        else{
            $pay_date = Carbon::createFromFormat('m/d/Y', $request->pay_date)->format('Y-m-d');
            $work_payment = new WorkPayment();
            $work_payment->user_id = $request->user_id;
            $work_payment->start_date = $request->startDate;
            $work_payment->end_date = $request->endDate;
            $work_payment->payment = $request->payment;
            $work_payment->pay_date = $pay_date;
            $work_payment->bonus = $request->bonus;
            $work_payment->balance = $request->balance;
            $work_payment->payable = $request->payable;
            $work_payment->save();
        }
        
        

        return Reply::redirect(route('admin.workPayment.index'), __('work Payment added'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->work_payment = WorkPayment::findOrFail($id);
        $this->startDate  = Carbon::parse($this->work_payment->start_date);
        $this->endDate = Carbon::parse($this->work_payment->end_date);
        $this->payDate = Carbon::parse($this->work_payment->pay_date);
        $this->employees = User::allEmployees();
        return view('admin.work-payment.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WorkPayment::destroy($id);
        return Reply::success(__('messages.workPaymentDeleted'));
    }
}
