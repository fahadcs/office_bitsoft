<?php

namespace App\Http\Controllers\Admin;

use App\Attendance;
use App\AttendanceSetting;
use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\Attendance\StoreAttendance;
use App\Leave;
use App\ModuleSetting;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use App\Payable;

class ManageAttendanceController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('app.menu.attendance');
        $this->pageIcon = 'icon-clock';

        if(!ModuleSetting::checkModule('attendance')){
            abort(403);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attendanceSettings = AttendanceSetting::first();
        $openDays = json_decode($attendanceSettings->office_open_days);
        $this->startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $this->endDate = Carbon::today()->timezone($this->global->timezone);
        $this->employees = User::allEmployees();
        $this->userId = User::first()->id;

        $this->totalWorkingDays = $this->startDate->diffInDaysFiltered(function(Carbon $date) use ($openDays){
            foreach($openDays as $day){
                if($date->dayOfWeek == $day){
                    return $date;
                }
            }
        }, $this->endDate);
        $this->daysPresent = Attendance::countDaysPresentByUser($this->startDate, $this->endDate, $this->userId);
        $this->daysLate = Attendance::countDaysLateByUser($this->startDate, $this->endDate, $this->userId);
        $this->halfDays = Attendance::countHalfDaysByUser($this->startDate, $this->endDate, $this->userId);
        return view('admin.attendance.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.attendance.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAttendance $request)
    {
       
        $date = Carbon::createFromFormat('m/d/Y', $request->date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat('h:i A', $request->clock_in_time, $this->global->timezone);
        $clockIn->setTimezone('UTC');
        $clockIn = $clockIn->format('H:i:s');
        if($request->clock_out_time != ''){
            $clockOut = Carbon::createFromFormat('h:i A', $request->clock_out_time, $this->global->timezone);
            $clockOut->setTimezone('UTC');
            $clockOut = $clockOut->format('H:i:s');
            $clockOut = $date.' '.$clockOut;
        }
        else{
            $clockOut = null;
        }
        
        
        $attendance = Attendance::where('user_id', $request->user_id)->where(DB::raw('DATE(`clock_in_time`)'), $date)->first();
        if(!is_null($attendance)){
            $attendance->update([
                'user_id' => $request->user_id,
                'clock_in_time' => $date.' '.$clockIn,
                'clock_in_ip' => $request->clock_in_ip,
                'clock_out_time' => $clockOut,
                'clock_out_ip' => $request->clock_out_ip,
                'working_from' => $request->working_from,
                'work_summary' => $request->work_summary,
                'late' => $request->late,
                'half_day' => $request->half_day
            ]);
        }else{
            Attendance::create([
                'user_id' => $request->user_id,
                'clock_in_time' => $date.' '.$clockIn,
                'clock_in_ip' => $request->clock_in_ip,
                'clock_out_time' => $clockOut,
                'clock_out_ip' => $request->clock_out_ip,
                'working_from' => $request->working_from,
                'work_summary' => $request->work_summary,
                'late' => $request->late,
                'half_day' => $request->half_day
            ]);
        }

        return Reply::success(__('messages.attendanceSaveSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Attendance::destroy($id);
        return Reply::success(__('messages.attendanceDelete'));
    }

    public function data(Request $request){
        $date = Carbon::createFromFormat('m/d/Y', $request->date)->format('Y-m-d');
        $attendances = Attendance::attendanceByDate($date);

        return Datatables::of($attendances)
            ->edit_column('id', function ($row) {
               
                return view('admin.attendance.attendance_list', ['row' => $row, 'global' => $this->global])->render();
            })
            ->remove_column('name')
            ->remove_column('clock_in_time')
            ->remove_column('clock_out_time')
            ->remove_column('image')
            ->remove_column('attendance_id')
            ->remove_column('working_from')
            ->remove_column('late')
            ->remove_column('half_day')
            ->make();
    }

    public function refreshCount($startDate = null, $end_date = null, $userId = null){
        $attendanceSettings = AttendanceSetting::first();
        $openDays = json_decode($attendanceSettings->office_open_days);
        $startDate = Carbon::createFromFormat('!Y-m-d', $startDate);
        $endDate = Carbon::createFromFormat('!Y-m-d', $end_date); //addDay(1) is hack to include end date
        $endDate2 = Carbon::createFromFormat('!Y-m-d', $end_date)->addDay(1);
        $totalWorkingDays = $startDate->diffInDaysFiltered(function(Carbon $date) use ($openDays){
            foreach($openDays as $day){
                if($date->dayOfWeek == $day){
                    return $date;
                }
            }
        }, $endDate2);
        $daysPresent = Attendance::countDaysPresentByUser($startDate, $endDate, $userId);
        $daysLate = Attendance::countDaysLateByUser($startDate, $endDate, $userId);
        $halfDays = Attendance::countHalfDaysByUser($startDate, $endDate, $userId);
        $totalminutes = Attendance::countTotalHoursByUser($startDate, $endDate, $userId, $this->global);
        $hours = floor( $totalminutes / 60 ); 
        $minute = $totalminutes % 60;

        $totalHours = $hours.':'.$minute;

        $daysAbsent = (($totalWorkingDays - $daysPresent) < 0) ? '0' : ($totalWorkingDays - $daysPresent);
        if($userId == 0){
            $all_users=Payable::distinct('user_id')->pluck('user_id');
            $current_payable = 0;
            $monthly_working_hours = 0;
            foreach ($all_users as $key => $value){
                
                $total_payable=Payable::where('user_id','=',$value)->whereDate('from_date', '<=', $endDate)->orderBy('from_date', 'DESC')->first();
                if($total_payable){
                    $current_payable += $total_payable->amount;
                }
                else{
                    $total_payable=Payable::where('user_id','=',$value)->whereDate('from_date', '>=', $endDate)->orderBy('from_date', 'ASC')->first();
                    $current_payable += $total_payable->amount;
                }
                
                $user = User::where('id','=',$value)->first();
                if($user){
                    $monthly_working_hours += $user->monthly_working_hours * 60;
                }
                
            }
        
            
            $hourly_rate = $current_payable / $monthly_working_hours;
           
            $amount_till_now = (int)($hourly_rate * $totalminutes);
        }
        else{
            $current_payable = 0;
            $amount_till_now = 0;
            $payable=Payable::where('user_id','=',$userId)->whereDate('from_date', '<=', $endDate)->orderBy('from_date', 'DESC')->first();
            if($payable){
                $current_payable += $payable->amount;
            }
            else{
                $payable=Payable::where('user_id','=',$userId)->whereDate('from_date', '>=', $endDate)->orderBy('from_date', 'ASC')->first();
                    if($payable){
                        $current_payable += $payable->amount;
                    }
                
            }
            if($payable){
                $user = User::where('id','=',$userId)->first();
                $monthly_working_hours = $user->monthly_working_hours * 60;
                $hourly_rate = $current_payable / $monthly_working_hours;
           
                $amount_till_now =  (int)($hourly_rate * $totalminutes);
            }
    
            
        }
        $current_payable = (int) $current_payable;   
        return Reply::dataOnly(['daysPresent' => $daysPresent,'current_payable' =>$current_payable,'amount_till_now' => $amount_till_now, 'daysLate' => $daysLate, 'totalHours' => $totalHours, 'halfDays' => $halfDays, 'totalWorkingDays' => $totalWorkingDays, 'absentDays' => $daysAbsent]);

    }

    public function employeeData($startDate = null, $endDate = null, $userId = null){
        $attendances = Attendance::userAttendanceByDate($startDate, $endDate, $userId);
        $presentDates = $attendances->pluck('clock_in_date');
        $startDate = Carbon::createFromFormat('!Y-m-d', $startDate);
        $endDate = Carbon::createFromFormat('!Y-m-d', $endDate);
        $leavesDates = Leave::where('user_id', $userId)
            ->where('leave_date', '>=', $startDate)
            ->where('leave_date', '<=', $endDate)
            ->where('status', 'approved')
            ->select('leave_date')
            ->get();
        $leaves = [];

        foreach ($leavesDates as $leavesDate) {
            array_push($leaves, $leavesDate->leave_date->format('Y-m-d'));
        }

        $employees = User::allEmployees();

        if($userId != 0){
            $view = view('admin.attendance.user_attendance_single',
            [
                'attendances' => $attendances, 'startDate' => $startDate,
                'endDate' => $endDate, 'presentDates' => $presentDates,
                'global' => $this->global, 'leavesDate' => $leaves,
                'employees' => $employees
            ]
            )->render();

            return Reply::dataOnly(['status' => 'success', 'data' => $view, 'view' => 'single']);
        }
        else{
            $view = view('admin.attendance.user_attendance',
            [
                'attendances' => $attendances, 'startDate' => $startDate,
                'endDate' => $endDate, 'presentDates' => $presentDates,
                'global' => $this->global, 'leavesDate' => $leaves,
                'employees' => $employees
            ]
             )->render();

            return Reply::dataOnly(['status' => 'success', 'data' => $view, 'view' => 'all']);
        }
        

    }

    public function attendanceByDate(){
        return view('admin.attendance.by_date', $this->data);
    }


    public function byDateData(Request $request){
        $date = Carbon::createFromFormat('m/d/Y', $request->date)->format('Y-m-d');
        $attendances = Attendance::attendanceByDate($date);

        return Datatables::of($attendances)
            ->edit_column('id', function ($row) {
                return view('admin.attendance.attendance_by_date_list', ['row' => $row, 'global' => $this->global])->render();
            })
            ->remove_column('name')
            ->remove_column('clock_in_time')
            ->remove_column('clock_out_time')
            ->remove_column('image')
            ->remove_column('attendance_id')
            ->remove_column('working_from')
            ->remove_column('late')
            ->remove_column('half_day')
            ->make();
    }

    public function dateAttendanceCount(Request $request){
        $date = Carbon::createFromFormat('m/d/Y', $request->date)->format('Y-m-d');

        $totalEmployees = count(User::allEmployees());
        $totalPresent = Attendance::where(DB::raw('DATE(`clock_in_time`)'), '=', $date)->count();
        $totalAbsent = ($totalEmployees-$totalPresent);

        return Reply::dataOnly(['status' => 'success', 'totalEmployees' => $totalEmployees, 'totalPresent' => $totalPresent, 'totalAbsent' => $totalAbsent]);
    }

}
