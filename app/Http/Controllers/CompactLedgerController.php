<?php

namespace App\Http\Controllers;
use App\Account;
use Auth;
use App\Helper\Reply;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Validator;
use App\VoucherDetail;
use App\voucher_master;
use Session;
use DB;
use Illuminate\Http\Request;

class CompactLedgerController extends AdminBaseController
{
    //

    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'Compact ledger';
        $this->pageIcon = 'icon-clock';
    }

    public function index(){
        $this->existing = null;
        $this->user = auth()->user();
        $this->accountname = Account::select('id', 'account_name', 'user_id')
        ->where([
            ['user_id', $this->user->id],
        ['is_parent', '=', true],
        ['parent_id','=','0']
        ])->get();
        return view('accounts.compact_ledger', $this->data);

    }


    public function data(Request $request){
        $this->startDate=$request->startDate;
        $this->endDate=$request->endDate;

                if($request->account==='0'&&empty($request->startDate)&&empty($request->endDate))
                {

                    $user = auth()->user();
                    //
                    $accounts = Account::select('id', 'account_name', 'user_id')
                    ->where([
                        ['user_id', $user->id],
                        ['is_parent', '=', true],
                        ['parent_id','!=','0']])->get();

                    return DataTables::of($accounts)
                    ->addColumn('debit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');


                        $vouchers = VoucherDetail::whereIn('account_id',$child_accounts)->get();                        $debit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $debit_sum += $value->debit;
                        }
                        return $debit_sum;
                    })
                    ->addColumn('credit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');

                        $vouchers = VoucherDetail::whereIn('account_id',$child_accounts)->get();
                        $credit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $credit_sum += $value->credit;
                        }
                        return $credit_sum;
                    })

                    ->rawColumns([ 'debit_sum', 'credit_sum'])
                    ->make(true);
                }
                elseif (empty($request->startDate)&&empty($request->endDate))
                {
                    $user = auth()->user();
                    //
                    $accounts = Account::select('id', 'account_name', 'user_id')
                    ->where([['user_id', $user->id],['is_parent', '=', true],['parent_id','=',$request->account]])->get();

                    return DataTables::of($accounts)
                    ->addColumn('debit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::whereIn('account_id',$child_accounts)->get();
                        $debit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $debit_sum += $value->debit;
                        }
                        return $debit_sum;
                    })
                    ->addColumn('credit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::whereIn('account_id',$child_accounts)->get();
                        $credit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $credit_sum += $value->credit;
                        }
                        return $credit_sum;
                    })

                    ->rawColumns([ 'debit_sum', 'credit_sum'])
                    ->make(true);
                }
                elseif (empty($request->account))
                {

                    $user = auth()->user();
                    //
                    $accounts = Account::select('id', 'account_name', 'user_id')
                    ->where([
                        ['user_id', $user->id],
                        ['is_parent', '=', true],
                        ['parent_id','!=','0']])->get();

                    return DataTables::of($accounts)
                    ->addColumn('debit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->join('voucher_masters', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
                        ->whereBetween('voucher_masters.date', [
                            date("Y-m-d", strtotime($this->startDate)),
                            date("Y-m-d", strtotime($this->endDate))])
                        ->whereIn('account_id',$child_accounts)->get();                        $debit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $debit_sum += $value->debit;
                        }
                        return $debit_sum;
                    })
                    ->addColumn('credit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->join('voucher_masters', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
                        ->whereBetween('voucher_masters.date', [
                            date("Y-m-d", strtotime($this->startDate)),
                            date("Y-m-d", strtotime($this->endDate))])
                        ->whereIn('account_id',$child_accounts)->get();
                        $credit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $credit_sum += $value->credit;
                        }
                        return $credit_sum;
                    })

                    ->rawColumns([ 'debit_sum', 'credit_sum'])
                    ->make(true);
                }
                else{
                    $user = auth()->user();
                    //
                    $accounts = Account::select('id', 'account_name', 'user_id')
                    ->where([['user_id', $user->id],['is_parent', '=', true],['parent_id','=',$request->account]])->get();

                    return DataTables::of($accounts)
                    ->addColumn('debit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->join('voucher_masters', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
                        ->whereBetween('voucher_masters.date', [
                            date("Y-m-d", strtotime($this->startDate)),
                            date("Y-m-d", strtotime($this->endDate))])
                        ->whereIn('account_id',$child_accounts)->get();
                        $debit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $debit_sum += $value->debit;
                        }
                        return $debit_sum;
                    })
                    ->addColumn('credit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->join('voucher_masters', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
                        ->whereBetween('voucher_masters.date', [
                            date("Y-m-d", strtotime($this->startDate)),
                            date("Y-m-d", strtotime($this->endDate))])
                        ->whereIn('account_id',$child_accounts)->get();                        $credit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $credit_sum += $value->credit;
                        }
                        return $credit_sum;
                    })

                    ->rawColumns([ 'debit_sum', 'credit_sum'])
                    ->make(true);
                }
    }
}
