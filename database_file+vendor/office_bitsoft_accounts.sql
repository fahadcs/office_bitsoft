-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint NOT NULL,
  `is_parent` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'Utilities Expense','Expense',0,1,'2020-06-29 04:01:14','2020-06-29 04:01:14',1),(2,'Elect exp','Expense',7,0,'2020-06-29 04:04:59','2020-06-29 04:04:59',1),(9,'Elect exp','Expense',1,0,'2020-07-02 07:28:22','2020-07-02 07:28:22',64),(10,'water exp','Expense',1,0,'2020-07-09 01:27:12','2020-07-09 01:27:12',64),(11,'Expenses','Expense',0,1,'2020-07-14 13:54:39','2020-07-14 13:54:39',1),(12,'Utilities Expenses','Expense',11,1,'2020-07-14 13:55:15','2020-07-14 13:55:15',1),(13,'Electricity Expense Home','Expense',12,0,'2020-07-14 13:55:51','2020-07-14 13:56:20',1),(14,'Internet Expense Home','Expense',12,0,'2020-07-14 14:02:51','2020-07-14 14:02:51',1),(15,'Electricity Expense Office','Expense',18,0,'2020-07-14 14:03:32','2020-07-14 14:07:21',1),(16,'Internet Expense Office','Expense',18,0,'2020-07-14 14:04:01','2020-07-14 14:06:48',1),(17,'Rent Expense Office','Expense',39,0,'2020-07-14 14:04:38','2020-08-31 13:47:50',1),(18,'Utilities Expenses Office','Expense',11,1,'2020-07-14 14:06:28','2020-07-14 14:06:28',1),(19,'Salaries Expense','Expense',11,1,'2020-07-14 14:16:24','2020-07-14 14:16:24',1),(20,'Mirza Ali Work Expense','Expense',19,0,'2020-07-14 14:16:39','2020-07-14 14:16:39',1),(21,'Moaviz Work Expense','Expense',19,0,'2020-07-14 14:17:06','2020-07-14 14:17:06',1),(22,'Akhtar Work Expense','Expense',19,0,'2020-07-14 14:17:27','2020-07-14 14:17:27',1),(23,'Sameer Work Expense','Expense',19,0,'2020-07-14 14:17:56','2020-07-14 14:17:56',1),(24,'Office Setup Expense','Expense',39,0,'2020-07-14 14:19:10','2020-08-31 13:26:50',1),(25,'Miscellaneous Office Expenses','Expense',39,0,'2020-07-14 14:19:40','2020-08-31 13:36:25',1),(26,'Pocket Money Expense','Expense',11,0,'2020-07-14 14:20:05','2020-07-14 14:20:05',1),(27,'Lunch Expense Office','Expense',39,0,'2020-07-14 14:20:35','2020-07-18 07:41:00',1),(28,'Naina Work Expense','Expense',32,0,'2020-07-14 14:24:09','2020-08-06 06:49:12',1),(29,'Payables','Liability',0,1,'2020-07-14 14:34:27','2020-07-14 14:34:27',1),(30,'Account Payables Self','Liability',29,1,'2020-07-14 14:34:47','2020-07-18 06:50:15',1),(31,'Account Payables Office','Liability',29,1,'2020-07-14 14:35:02','2020-07-18 06:51:08',1),(32,'Blogging Expense','Expense',11,1,'2020-07-14 14:40:21','2020-07-18 06:23:07',1),(33,'Mystudyabroadtips exp','Expense',32,0,'2020-07-14 14:40:42','2020-07-14 14:40:42',1),(34,'Revenue Account','Revenue',0,1,'2020-07-17 06:46:11','2020-07-17 06:46:11',1),(35,'87 Revenue','Revenue',36,0,'2020-07-17 06:46:48','2020-07-18 06:25:19',1),(36,'Freelance Revenue','Revenue',34,1,'2020-07-18 06:25:03','2020-07-18 06:25:03',1),(37,'Utilities Payables','Liability',31,0,'2020-07-18 06:46:25','2020-07-18 06:51:44',1),(38,'Bike Payables','Liability',30,0,'2020-07-18 06:46:51','2020-07-18 06:56:04',1),(39,'Office Expenses','Expense',11,1,'2020-07-18 07:40:38','2020-07-18 07:40:38',1),(40,'utility','Expense',0,1,'2020-07-22 01:30:13','2020-07-22 01:30:13',67),(43,'87 Revenue Personal','Revenue',36,0,'2020-07-29 05:38:24','2020-07-29 05:38:24',1),(44,'kitchencookingkit.com exp','Expense',32,0,'2020-07-29 09:14:54','2020-07-29 09:14:54',1),(45,'Receivables','Assets',0,1,'2020-07-29 13:51:39','2020-07-29 13:51:39',1),(46,'Receivables Office','Assets',45,1,'2020-07-29 13:52:22','2020-07-29 13:52:36',1),(47,'Receivables Self','Assets',45,1,'2020-07-29 13:53:03','2020-07-29 13:53:03',1),(48,'Wife Receivables','Assets',47,0,'2020-07-29 13:57:55','2020-07-29 13:57:55',1),(49,'Borrow Receivables','Assets',47,0,'2020-07-29 13:58:38','2020-07-29 13:58:38',1),(50,'Mehwish Work Expense','Expense',32,0,'2020-07-30 09:50:02','2020-08-06 06:48:59',1),(51,'Hosting Expense','Expense',32,0,'2020-08-04 06:30:14','2020-08-04 06:30:14',1),(52,'KW everywhere Expense','Expense',32,0,'2020-08-04 06:30:34','2020-08-04 06:30:34',1),(53,'Bank Accounts','Assets',0,1,'2020-08-10 06:03:28','2020-08-10 06:03:28',1),(54,'MZ LHR','Assets',53,0,'2020-08-10 06:03:40','2020-08-10 06:03:40',1),(55,'Cash','Assets',0,1,'2020-08-10 06:03:53','2020-08-10 06:03:53',1),(56,'Cash-In-Hand','Assets',55,0,'2020-08-10 06:04:08','2020-08-10 06:04:08',1),(57,'Personal Expenses','Expense',11,1,'2020-08-10 06:04:48','2020-08-10 06:04:48',1),(58,'Siddiq Expenses','Expense',57,0,'2020-08-11 09:56:44','2020-08-11 09:56:44',1),(59,'Saving Accounts','Assets',0,1,'2020-08-19 05:52:50','2020-08-19 05:52:50',1),(60,'A Saving','Assets',59,0,'2020-08-19 05:53:06','2020-08-19 05:53:06',1),(61,'SEO Tools','Expense',32,0,'2020-08-19 06:38:05','2020-08-19 06:38:05',1),(62,'Lovingdollbeauty Exp','Expense',32,0,'2020-08-25 05:37:09','2020-08-25 05:37:09',1),(63,'Direct Revenue','Revenue',36,0,'2020-08-26 16:26:11','2020-08-26 16:26:11',1),(64,'Sheraz Work Expense','Expense',32,0,'2020-09-01 06:28:37','2020-09-01 06:28:37',1),(65,'Water Expense Office','Expense',39,0,'2020-09-04 06:31:02','2020-09-04 06:31:02',1),(66,'Borrow Receivable Adjusted','Assets',45,1,'2020-09-05 04:37:45','2020-09-05 04:37:45',1),(67,'Farooq Receivables','Assets',66,0,'2020-09-05 04:38:11','2020-09-05 04:38:11',1),(68,'Moaviz Receivables','Assets',66,0,'2020-09-05 04:38:34','2020-09-05 04:38:34',1),(69,'DAD Receiveables','Assets',66,0,'2020-09-05 04:38:51','2020-09-05 04:38:51',1),(70,'MOM Receivables','Assets',66,0,'2020-09-05 04:39:08','2020-09-05 04:39:08',1),(71,'Ali Receivables','Assets',66,0,'2020-09-05 04:44:01','2020-09-05 04:44:01',1),(72,'Muaviya Rec','Assets',47,0,'2020-09-09 04:18:29','2020-09-09 04:18:29',1),(73,'Usman Receivable','Assets',66,0,'2020-09-18 13:31:07','2020-09-18 13:31:07',1),(74,'MZ MUL','Assets',53,0,'2020-09-21 10:44:32','2020-09-21 10:44:32',1),(75,'HBL MUL','Assets',53,0,'2020-09-21 14:04:52','2020-09-21 14:04:52',1),(76,'abc','Assets',0,1,'2020-10-02 03:56:57','2020-10-02 03:56:57',72),(77,'abc2','Assets',76,0,'2020-10-02 03:57:11','2020-10-02 03:57:11',72),(78,'demo','Assets',76,0,'2020-10-02 03:57:28','2020-10-02 03:57:28',72),(79,'Wali Shah Work Expense','Expense',19,0,'2020-10-05 06:53:31','2020-10-05 06:53:31',1),(80,'Miscellaneous Home Expense','Expense',57,0,'2020-10-11 15:36:39','2020-10-11 15:36:39',1),(81,'Fahad Work Expense','Expense',19,0,'2020-11-07 10:28:23','2020-11-07 10:28:23',1),(82,'Investment Account','Capital',0,1,'2021-01-14 11:06:21','2021-01-14 11:06:21',1),(83,'Begum Investment Account','Capital',82,0,'2021-01-14 11:07:33','2021-01-14 11:07:33',1),(84,'Savings','Capital',0,1,'2021-01-14 11:30:05','2021-01-14 11:30:05',1),(85,'Office Savings','Capital',84,0,'2021-01-14 11:30:34','2021-01-14 11:30:34',1),(86,'elec','Expense',40,0,'2021-02-06 12:24:09','2021-02-06 12:24:09',67),(87,'zulqarnain work exp','Expense',32,0,'2021-02-17 07:58:10','2021-02-17 07:58:10',1),(88,'Office Investment Account','Assets',82,0,'2021-02-28 12:50:48','2021-02-28 12:50:48',1),(89,'hamza work expense','Expense',32,0,'2021-03-15 05:45:56','2021-03-15 05:46:42',1),(90,'Farooq Payables','Liability',30,0,'2021-03-15 06:15:26','2021-03-15 06:15:26',1),(91,'Cash-In-Office','Assets',55,0,'2021-03-30 09:12:34','2021-03-30 09:12:34',1),(92,'Siddiq Work Expense','Expense',19,0,'2021-03-30 09:13:00','2021-03-30 09:13:00',1),(93,'draz revenue','Revenue',34,0,'2021-05-21 09:12:50','2021-05-21 09:12:50',1),(94,'Kishan Work Expense','Expense',19,0,'2021-06-18 06:39:27','2021-06-18 06:39:27',1),(95,'Ahmad Yar Work Expense','Expense',19,0,'2021-06-18 06:42:28','2021-06-18 06:42:28',1),(96,'5rr revenue','Revenue',36,0,'2021-08-10 10:22:04','2021-08-10 10:22:04',1),(97,'Office Expansion Exp','Expense',39,0,'2021-08-10 10:35:35','2021-08-10 10:35:35',1),(98,'Blogging Revenue','Assets',34,1,'2021-08-30 04:11:30','2021-08-30 04:11:30',1),(99,'KCK Revenue','Assets',98,0,'2021-08-30 04:12:08','2021-08-30 04:12:08',1),(100,'Umar Waqas Work Expense','Expense',19,0,'2021-08-31 09:02:15','2021-08-31 09:02:15',1),(101,'Zaravar Work Expense','Expense',19,0,'2021-08-31 09:32:23','2021-08-31 09:32:23',1),(102,'Asad Work Expense','Expense',11,0,'2021-08-31 09:32:53','2021-08-31 09:32:53',1),(103,'Babar Work Expense','Expense',19,0,'2021-08-31 09:33:16','2021-08-31 09:33:16',1),(104,'Waqas Work Expense','Expense',19,0,'2021-08-31 09:33:43','2021-08-31 09:33:43',1),(107,'Jameel Bhai Work Expense','Expense',19,0,'2021-09-15 04:41:52','2021-09-15 04:41:52',1);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:12:59
