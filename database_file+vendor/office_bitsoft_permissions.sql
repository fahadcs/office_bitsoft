-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`),
  KEY `permissions_module_id_foreign` (`module_id`),
  CONSTRAINT `permissions_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'add_clients','Add Clients',NULL,1,NULL,NULL),(2,'view_clients','View Clients',NULL,1,NULL,NULL),(3,'edit_clients','Edit Clients',NULL,1,NULL,NULL),(4,'delete_clients','Delete Clients',NULL,1,NULL,NULL),(5,'add_employees','Add Employees',NULL,2,NULL,NULL),(6,'view_employees','View Employees',NULL,2,NULL,NULL),(7,'edit_employees','Edit Employees',NULL,2,NULL,NULL),(8,'delete_employees','Delete Employees',NULL,2,NULL,NULL),(9,'add_projects','Add Project',NULL,3,NULL,NULL),(10,'view_projects','View Project',NULL,3,NULL,NULL),(11,'edit_projects','Edit Project',NULL,3,NULL,NULL),(12,'delete_projects','Delete Project',NULL,3,NULL,NULL),(13,'add_attendance','Add Attendance',NULL,4,NULL,NULL),(14,'view_attendance','View Attendance',NULL,4,NULL,NULL),(15,'add_tasks','Add Tasks',NULL,5,NULL,NULL),(16,'view_tasks','View Tasks',NULL,5,NULL,NULL),(17,'edit_tasks','Edit Tasks',NULL,5,NULL,NULL),(18,'delete_tasks','Delete Tasks',NULL,5,NULL,NULL),(19,'add_estimates','Add Estimates',NULL,6,NULL,NULL),(20,'view_estimates','View Estimates',NULL,6,NULL,NULL),(21,'edit_estimates','Edit Estimates',NULL,6,NULL,NULL),(22,'delete_estimates','Delete Estimates',NULL,6,NULL,NULL),(23,'add_invoices','Add Invoices',NULL,7,NULL,NULL),(24,'view_invoices','View Invoices',NULL,7,NULL,NULL),(25,'edit_invoices','Edit Invoices',NULL,7,NULL,NULL),(26,'delete_invoices','Delete Invoices',NULL,7,NULL,NULL),(27,'add_payments','Add Payments',NULL,8,NULL,NULL),(28,'view_payments','View Payments',NULL,8,NULL,NULL),(29,'edit_payments','Edit Payments',NULL,8,NULL,NULL),(30,'delete_payments','Delete Payments',NULL,8,NULL,NULL),(31,'add_timelogs','Add Timelogs',NULL,9,NULL,NULL),(32,'view_timelogs','View Timelogs',NULL,9,NULL,NULL),(33,'edit_timelogs','Edit Timelogs',NULL,9,NULL,NULL),(34,'delete_timelogs','Delete Timelogs',NULL,9,NULL,NULL),(35,'add_tickets','Add Tickets',NULL,10,NULL,NULL),(36,'view_tickets','View Tickets',NULL,10,NULL,NULL),(37,'edit_tickets','Edit Tickets',NULL,10,NULL,NULL),(38,'delete_tickets','Delete Tickets',NULL,10,NULL,NULL),(39,'add_events','Add Events',NULL,11,NULL,NULL),(40,'view_events','View Events',NULL,11,NULL,NULL),(41,'edit_events','Edit Events',NULL,11,NULL,NULL),(42,'delete_events','Delete Events',NULL,11,NULL,NULL),(43,'add_notice','Add Notice',NULL,12,NULL,NULL),(44,'view_notice','View Notice',NULL,12,NULL,NULL),(45,'edit_notice','Edit Notice',NULL,12,NULL,NULL),(46,'delete_notice','Delete Notice',NULL,12,NULL,NULL),(47,'add_leave','Add Leave',NULL,13,NULL,NULL),(48,'view_leave','View Leave',NULL,13,NULL,NULL),(49,'edit_leave','Edit Leave',NULL,13,NULL,NULL),(50,'delete_leave','Delete Leave',NULL,13,NULL,NULL),(51,'add_lead','Add Lead',NULL,14,NULL,NULL),(52,'view_lead','View Lead',NULL,14,NULL,NULL),(53,'edit_lead','Edit Lead',NULL,14,NULL,NULL),(54,'delete_lead','Delete Lead',NULL,14,NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:07:32
