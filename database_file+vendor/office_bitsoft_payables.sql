-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `payables`
--

DROP TABLE IF EXISTS `payables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payables` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `from_date` datetime DEFAULT NULL,
  `amount` decimal(9,3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payables_user_id_foreign` (`user_id`),
  CONSTRAINT `payables_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payables`
--

LOCK TABLES `payables` WRITE;
/*!40000 ALTER TABLE `payables` DISABLE KEYS */;
INSERT INTO `payables` VALUES (2,23,'2021-06-15 00:00:00',0.000,'2021-06-15 07:27:55','2021-06-15 07:27:55'),(23,1,'2021-06-01 00:00:00',100000.000,'2021-07-16 03:51:18','2021-07-16 03:51:18'),(24,12,'2020-12-01 00:00:00',20000.000,'2021-07-16 03:51:27','2021-07-16 03:51:27'),(25,12,'2021-02-01 00:00:00',30000.000,'2021-07-16 03:51:27','2021-07-16 03:51:27'),(26,12,'2021-07-01 00:00:00',40000.000,'2021-07-16 03:51:27','2021-07-16 03:51:27'),(32,25,'2021-06-12 00:00:00',5.000,'2021-07-31 04:52:37','2021-07-31 04:52:37'),(34,27,'2021-08-01 00:00:00',2000.000,'2021-08-23 03:41:41','2021-08-23 03:41:41'),(35,28,'2021-08-25 00:00:00',2000.000,'2021-08-25 05:39:21','2021-08-25 05:39:21'),(36,29,'2021-08-26 00:00:00',3000.000,'2021-08-26 03:41:12','2021-08-26 03:41:12'),(37,26,'2021-08-01 00:00:00',4000.000,'2021-09-13 06:25:58','2021-09-13 06:25:58'),(38,19,'2021-01-01 00:00:00',12500.000,'2021-09-13 06:27:38','2021-09-13 06:27:38'),(39,19,'2021-03-01 00:00:00',17500.000,'2021-09-13 06:27:38','2021-09-13 06:27:38'),(40,19,'2021-07-01 00:00:00',26500.000,'2021-09-13 06:27:38','2021-09-13 06:27:38'),(41,19,'2021-09-01 00:00:00',38000.000,'2021-09-13 06:27:38','2021-09-13 06:27:38'),(42,22,'2021-02-01 00:00:00',9000.000,'2021-09-13 06:28:10','2021-09-13 06:28:10'),(43,22,'2021-03-01 00:00:00',11000.000,'2021-09-13 06:28:10','2021-09-13 06:28:10'),(44,22,'2021-08-01 00:00:00',15000.000,'2021-09-13 06:28:10','2021-09-13 06:28:10'),(46,21,'2021-08-01 00:00:00',18000.000,'2021-09-13 06:30:17','2021-09-13 06:30:17'),(47,30,'2021-09-01 00:00:00',8000.000,'2021-09-13 06:31:26','2021-09-13 06:31:26'),(48,31,'2021-08-01 00:00:00',2000.000,'2021-09-13 06:33:16','2021-09-13 06:33:16'),(49,31,'2021-09-01 00:00:00',3000.000,'2021-09-13 06:33:16','2021-09-13 06:33:16'),(51,16,'2021-09-01 00:00:00',1500.000,'2021-09-27 04:42:22','2021-09-27 04:42:22');
/*!40000 ALTER TABLE `payables` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:11:12
