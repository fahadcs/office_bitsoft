-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `users_chat`
--

DROP TABLE IF EXISTS `users_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_chat` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_one` int unsigned NOT NULL,
  `user_id` int unsigned NOT NULL,
  `message` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `from` int unsigned DEFAULT NULL,
  `to` int unsigned DEFAULT NULL,
  `message_seen` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_chat_user_one_foreign` (`user_one`),
  KEY `users_chat_user_id_foreign` (`user_id`),
  KEY `users_chat_from_foreign` (`from`),
  KEY `users_chat_to_foreign` (`to`),
  CONSTRAINT `users_chat_from_foreign` FOREIGN KEY (`from`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_chat_to_foreign` FOREIGN KEY (`to`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_chat_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_chat_user_one_foreign` FOREIGN KEY (`user_one`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_chat`
--

LOCK TABLES `users_chat` WRITE;
/*!40000 ALTER TABLE `users_chat` DISABLE KEYS */;
INSERT INTO `users_chat` VALUES (2,1,5,'sftp\r\nkiwiforgmail-dev.serverdatahost.com\r\nLogin:kiwiforgmail.com-virtual\r\nPass:oiaalsdfkjosw',1,5,'yes','2019-09-26 19:34:24','2020-02-13 11:42:13'),(3,5,1,'OK',5,1,'yes','2019-09-26 19:37:12','2019-10-21 13:32:46'),(4,1,2,'WHM creds for a-paea \r\n\r\nhttps://207.210.201.18:2087\r\nUsername: root\r\nPassword: Tj5VvXyjyjpgVUj3\r\n\r\na-paea Admin login\r\nhttps://www.a-paea.org/admin/login\r\nusername:hamid93pk@gmail.com\r\npassw',1,2,'yes','2019-09-29 09:57:50','2019-10-20 13:43:42'),(5,2,1,'okay brosss',2,1,'yes','2019-09-29 10:18:30','2019-10-16 18:29:57'),(10,1,2,'Creds',1,2,'yes','2019-10-16 17:07:37','2019-10-20 13:43:42'),(11,1,2,'web url: https://threepoint-dev.serverdatahost.com  phpmyadmin: https://threepts_com_virtual.db3.serverdatahost.com/phpmyadmin host: 127.0.0.1 db:threepts_com_virtual l:threepts_co57 p:weruoi',1,2,'yes','2019-10-16 17:07:47','2019-10-20 13:43:42'),(12,1,2,'web url: https://threepoint-dev.serverdatahost.com  phpmyadmin: https://threepts_com_virtual.db3.serverdatahost.com/phpmyadmin host: 127.0.0.1 db:threepts_com_virtual l:threepts_co57 p:weruoi',1,2,'yes','2019-10-16 17:07:59','2019-10-20 13:43:42'),(13,1,2,'site login adrianvfx@gmail.com Adm123!@#',1,2,'yes','2019-10-16 17:08:20','2019-10-20 13:43:42'),(14,1,7,'Please note the credentials for the donalisa https://thedonalisa.com/wp-admin admin@thedonalisa.com @FA&%5L7yg',1,7,'yes','2019-10-24 14:38:16','2019-10-24 16:40:46'),(15,1,14,'Credentials:\r\nhttps://www.lovingdollbeauty.com/wp-admin Username: admin@bitsoftsol.com Pass:&RMQM3IDer',1,14,'yes','2020-01-12 19:31:30','2020-03-17 12:01:37'),(16,1,14,'L: yk131672@gmail.com	P:Seotop@305 site: https://www.canva.com/',1,14,'yes','2020-01-14 18:16:54','2020-03-17 12:01:37');
/*!40000 ALTER TABLE `users_chat` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:09:33
