-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `employee_details`
--

DROP TABLE IF EXISTS `employee_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `job_title` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `hourly_rate` int DEFAULT NULL,
  `slack_username` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `joining_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_remote` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_details_slack_username_unique` (`slack_username`),
  KEY `employee_details_user_id_foreign` (`user_id`),
  CONSTRAINT `employee_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_details`
--

LOCK TABLES `employee_details` WRITE;
/*!40000 ALTER TABLE `employee_details` DISABLE KEYS */;
INSERT INTO `employee_details` VALUES (1,1,'Project Manager','address',50,NULL,'2019-09-16 16:13:02','2021-07-16 03:51:18','2019-09-15 22:00:00',0),(2,2,'Web Developer',NULL,1,NULL,'2019-09-16 19:42:59','2019-09-16 19:42:59','2019-09-15 22:00:00',NULL),(3,3,'Flutter Developer',NULL,0,NULL,'2019-09-16 19:45:51','2019-09-16 19:45:51','2019-09-16 22:00:00',NULL),(5,5,'Front End Developer','House 1848, Chori Saray Bazar I/S Dehli Gate, Multan',1,NULL,'2019-09-21 18:36:44','2020-02-13 11:19:01','2019-09-20 22:00:00',NULL),(6,6,'WordPress Developer',NULL,1,NULL,'2019-10-04 17:43:55','2019-10-04 17:43:55','2019-10-03 22:00:00',NULL),(7,7,'Content Writer',NULL,1,NULL,'2019-10-12 17:15:23','2019-10-12 17:15:23','2019-10-11 22:00:00',NULL),(8,8,'Mobile Developer','Lahore',1,NULL,'2019-11-05 17:55:15','2019-11-05 17:55:15','2019-11-04 23:00:00',NULL),(10,10,'Web Developer',NULL,150,NULL,'2019-11-28 17:50:12','2019-11-28 17:50:12','2019-11-27 23:00:00',NULL),(11,11,'Full stack Developer',NULL,1,NULL,'2019-12-04 19:00:25','2019-12-04 19:00:25','2019-12-03 23:00:00',NULL),(12,12,'Web Developer',NULL,1,NULL,'2019-12-09 16:10:32','2021-07-16 03:51:27','2019-12-08 23:00:00',0),(14,14,'Content Writer','Multan',1,NULL,'2020-01-12 19:08:27','2020-01-12 19:08:27','2020-01-11 23:00:00',NULL),(15,15,'Writer',NULL,1,NULL,'2020-03-13 15:01:33','2020-03-13 15:01:33','2020-03-12 23:00:00',NULL),(16,16,'Backlinker',NULL,1,NULL,'2020-03-21 15:04:35','2021-09-27 04:42:22','2020-03-20 23:00:00',0),(17,17,'Web Developer',NULL,1,NULL,'2020-04-24 20:17:55','2020-04-24 20:17:55','2020-04-23 22:00:00',NULL),(18,18,'Web Developer Intern',NULL,1,NULL,'2020-06-24 12:34:07','2020-06-24 12:34:07','2020-06-18 22:00:00',NULL),(19,19,'Web Developer','X.Y.Z',1,NULL,'2020-06-24 12:35:46','2021-09-13 06:27:38','2020-06-22 22:00:00',0),(20,20,'Web Developer Intern',NULL,1,NULL,'2020-06-24 12:41:43','2020-06-24 12:41:43','2020-06-14 22:00:00',NULL),(21,21,'Article/Content Writer',NULL,170,NULL,'2020-07-14 13:22:38','2021-09-13 06:30:17','2020-07-13 22:00:00',1),(22,22,'SEO expert',NULL,1,NULL,'2021-02-03 16:18:24','2021-09-13 06:28:10','2021-02-01 23:00:00',0),(23,23,'Full Stack Django Developer',NULL,8,NULL,'2021-05-31 06:34:06','2021-06-15 07:27:55','2021-05-30 22:00:00',1),(25,25,'Django Python Developer','Multan Pakistan',5,NULL,'2021-06-12 05:25:54','2021-07-31 04:52:37','2021-06-11 22:00:00',1),(26,26,'SEO Expert',NULL,1,NULL,'2021-08-02 05:56:00','2021-09-13 06:25:58','2021-07-31 22:00:00',1),(27,27,'Full Stack Web Developer',NULL,1,NULL,'2021-08-23 03:41:41','2021-08-23 03:41:41','2021-08-16 22:00:00',0),(28,28,'SEO Expert',NULL,1,NULL,'2021-08-25 05:39:21','2021-08-25 05:39:21','2021-08-24 22:00:00',0),(29,29,'Full Stack Web Developer',NULL,1,NULL,'2021-08-26 03:41:12','2021-08-26 03:41:12','2021-08-25 22:00:00',0),(30,30,'Janitor',NULL,1,NULL,'2021-09-13 06:31:26','2021-09-13 06:31:26','2021-08-15 22:00:00',0),(31,31,'Roti Lunch Expense',NULL,1,NULL,'2021-09-13 06:33:16','2021-09-13 06:33:16','2021-07-31 22:00:00',1);
/*!40000 ALTER TABLE `employee_details` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:12:02
