-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `project_activity`
--

DROP TABLE IF EXISTS `project_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_activity` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int unsigned NOT NULL,
  `activity` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_activity_project_id_foreign` (`project_id`),
  CONSTRAINT `project_activity_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=693 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_activity`
--

LOCK TABLES `project_activity` WRITE;
/*!40000 ALTER TABLE `project_activity` DISABLE KEYS */;
INSERT INTO `project_activity` VALUES (596,44,'Trusting Beaver Django added as new project.','2021-05-31 06:33:09','2021-05-31 06:33:09'),(597,44,'Kishan is added as project member.','2021-05-31 06:34:32','2021-05-31 06:34:32'),(598,44,'New file uploaded to the project.','2021-05-31 06:38:16','2021-05-31 06:38:16'),(599,44,'Hafiz Siddiq Umer is added as project member.','2021-05-31 06:39:13','2021-05-31 06:39:13'),(600,45,'Self-Bitsoft Creds added as new project.','2021-05-31 06:40:44','2021-05-31 06:40:44'),(601,45,'Fahad is added as project member.','2021-05-31 06:41:04','2021-05-31 06:41:04'),(602,45,'Akhtar is added as project member.','2021-05-31 06:41:04','2021-05-31 06:41:04'),(603,46,'Tocino added as new project.','2021-05-31 06:43:37','2021-05-31 06:43:37'),(604,46,'Hafiz Siddiq Umer is added as project member.','2021-05-31 06:43:49','2021-05-31 06:43:49'),(605,44,'New task added to the project.','2021-06-02 06:07:28','2021-06-02 06:07:28'),(606,44,'New file uploaded to the project.','2021-06-02 06:08:15','2021-06-02 06:08:15'),(607,44,'New task added to the project.','2021-06-08 06:19:08','2021-06-08 06:19:08'),(608,46,'Ahmad Yar is added as project member.','2021-06-12 05:27:53','2021-06-12 05:27:53'),(609,44,'Timer started by Hafiz Siddiq Umer','2021-06-14 03:53:49','2021-06-14 03:53:49'),(610,44,'Timer stopped by Hafiz Siddiq Umer','2021-06-14 04:05:44','2021-06-14 04:05:44'),(611,44,'Timer started by Hafiz Siddiq Umer','2021-06-14 04:06:06','2021-06-14 04:06:06'),(612,44,'Timer stopped by Hafiz Siddiq Umer','2021-06-14 04:10:58','2021-06-14 04:10:58'),(613,47,'Matrix Switch added as new project.','2021-06-17 04:37:35','2021-06-17 04:37:35'),(614,47,'Hafiz Siddiq Umer is added as project member.','2021-06-17 04:37:55','2021-06-17 04:37:55'),(615,47,'Ahmad Yar is added as project member.','2021-06-17 04:37:55','2021-06-17 04:37:55'),(616,46,'New task added to the project.','2021-06-24 05:14:15','2021-06-24 05:14:15'),(617,44,'Django Admin project details updated.','2021-06-29 04:32:51','2021-06-29 04:32:51'),(618,44,'Ahmad Yar is added as project member.','2021-06-29 04:33:26','2021-06-29 04:33:26'),(619,44,'Timer started by Ahmad Yar','2021-06-30 04:07:38','2021-06-30 04:07:38'),(620,44,'Timer stopped by Ahmad Yar','2021-06-30 04:34:39','2021-06-30 04:34:39'),(621,44,'Timer started by Ahmad Yar','2021-06-30 05:16:59','2021-06-30 05:16:59'),(622,44,'Timer stopped by Ahmad Yar','2021-06-30 06:18:04','2021-06-30 06:18:04'),(623,44,'Timer started by Ahmad Yar','2021-06-30 07:00:30','2021-06-30 07:00:30'),(624,44,'Timer stopped by Ahmad Yar','2021-06-30 10:24:20','2021-06-30 10:24:20'),(625,44,'Timer started by Ahmad Yar','2021-06-30 12:47:49','2021-06-30 12:47:49'),(626,44,'Timer stopped by Ahmad Yar','2021-06-30 13:12:54','2021-06-30 13:12:54'),(627,46,'Timer started by Hafiz Siddiq Umer','2021-06-30 17:42:50','2021-06-30 17:42:50'),(628,46,'Timer stopped by Hafiz Siddiq Umer','2021-06-30 17:43:22','2021-06-30 17:43:22'),(629,48,'Store Website Gaminglight added as new project.','2021-07-02 04:19:40','2021-07-02 04:19:40'),(630,48,'Hafiz Siddiq Umer is added as project member.','2021-07-02 04:19:57','2021-07-02 04:19:57'),(631,48,'Fahad is added as project member.','2021-07-02 04:19:58','2021-07-02 04:19:58'),(632,47,'Timer started by Ahmad Yar','2021-07-03 05:43:47','2021-07-03 05:43:47'),(633,47,'Timer started by Ahmad Yar','2021-07-03 06:00:03','2021-07-03 06:00:03'),(634,47,'Timer stopped by Ahmad Yar','2021-07-03 06:00:08','2021-07-03 06:00:08'),(635,47,'Timer stopped by Ahmad Yar','2021-07-05 06:05:50','2021-07-05 06:05:50'),(636,47,'Timer started by Ahmad Yar','2021-07-05 07:30:35','2021-07-05 07:30:35'),(637,47,'Timer stopped by Ahmad Yar','2021-07-05 10:22:16','2021-07-05 10:22:16'),(638,47,'Timer started by Ahmad Yar','2021-07-05 10:52:39','2021-07-05 10:52:39'),(639,47,'Timer stopped by Ahmad Yar','2021-07-05 11:38:27','2021-07-05 11:38:27'),(640,47,'Timer started by Ahmad Yar','2021-07-06 04:05:04','2021-07-06 04:05:04'),(641,47,'Timer stopped by Ahmad Yar','2021-07-06 04:44:10','2021-07-06 04:44:10'),(642,47,'Timer started by Ahmad Yar','2021-07-06 05:01:15','2021-07-06 05:01:15'),(643,47,'Timer stopped by Ahmad Yar','2021-07-06 06:22:02','2021-07-06 06:22:02'),(644,47,'Timer started by Ahmad Yar','2021-07-06 07:23:32','2021-07-06 07:23:32'),(645,47,'Timer stopped by Ahmad Yar','2021-07-06 08:21:42','2021-07-06 08:21:42'),(646,47,'Timer started by Ahmad Yar','2021-07-06 08:46:32','2021-07-06 08:46:32'),(647,47,'Timer stopped by Ahmad Yar','2021-07-06 10:24:30','2021-07-06 10:24:30'),(648,47,'Timer started by Ahmad Yar','2021-07-06 11:06:31','2021-07-06 11:06:31'),(649,47,'Timer stopped by Ahmad Yar','2021-07-06 11:32:19','2021-07-06 11:32:19'),(650,47,'Timer started by Ahmad Yar','2021-07-07 03:58:32','2021-07-07 03:58:32'),(651,47,'Timer stopped by Ahmad Yar','2021-07-07 04:25:49','2021-07-07 04:25:49'),(652,47,'Timer started by Ahmad Yar','2021-07-07 04:34:48','2021-07-07 04:34:48'),(653,47,'Timer stopped by Ahmad Yar','2021-07-07 06:19:46','2021-07-07 06:19:46'),(654,47,'Timer started by Ahmad Yar','2021-07-07 06:56:37','2021-07-07 06:56:37'),(655,47,'Timer stopped by Ahmad Yar','2021-07-07 08:11:03','2021-07-07 08:11:03'),(656,47,'Timer started by Ahmad Yar','2021-07-07 08:33:47','2021-07-07 08:33:47'),(657,47,'Timer stopped by Ahmad Yar','2021-07-07 08:49:53','2021-07-07 08:49:53'),(658,46,'Timer started by Ahmad Yar','2021-07-08 05:15:32','2021-07-08 05:15:32'),(659,46,'Timer stopped by Ahmad Yar','2021-07-08 06:15:11','2021-07-08 06:15:11'),(660,47,'Timer started by Ahmad Yar','2021-07-08 06:54:16','2021-07-08 06:54:16'),(661,47,'Timer stopped by Ahmad Yar','2021-07-08 08:15:07','2021-07-08 08:15:07'),(662,47,'Timer started by Ahmad Yar','2021-07-12 07:15:43','2021-07-12 07:15:43'),(663,47,'Timer stopped by Ahmad Yar','2021-07-12 08:17:19','2021-07-12 08:17:19'),(664,47,'Timer started by Ahmad Yar','2021-07-13 07:15:45','2021-07-13 07:15:45'),(665,47,'Timer stopped by Ahmad Yar','2021-07-13 07:37:15','2021-07-13 07:37:15'),(666,47,'Timer started by Ahmad Yar','2021-07-13 07:53:49','2021-07-13 07:53:49'),(667,47,'Timer stopped by Ahmad Yar','2021-07-13 10:22:36','2021-07-13 10:22:36'),(668,49,'Home Work Hamza added as new project.','2021-07-27 10:46:03','2021-07-27 10:46:03'),(669,49,'Hamza is added as project member.','2021-07-27 10:46:19','2021-07-27 10:46:19'),(670,50,'Jkp_portal added as new project.','2021-07-30 05:12:32','2021-07-30 05:12:32'),(671,50,'Fahad is added as project member.','2021-07-30 05:12:54','2021-07-30 05:12:54'),(672,50,'Akhtar is added as project member.','2021-07-30 05:12:54','2021-07-30 05:12:54'),(673,50,'New task added to the project.','2021-07-30 05:22:04','2021-07-30 05:22:04'),(674,51,'Backlinks KCK added as new project.','2021-08-02 06:00:40','2021-08-02 06:00:40'),(675,51,'Farhan is added as project member.','2021-08-02 06:01:12','2021-08-02 06:01:12'),(676,52,'Backlinks LDB added as new project.','2021-08-02 06:07:37','2021-08-02 06:07:37'),(677,52,'Farhan is added as project member.','2021-08-02 06:08:07','2021-08-02 06:08:07'),(678,50,'New task added to the project.','2021-08-03 05:21:29','2021-08-03 05:21:29'),(679,50,'New task added to the project.','2021-08-03 05:35:56','2021-08-03 05:35:56'),(680,52,'Backlinks LDB project details updated.','2021-08-03 05:37:07','2021-08-03 05:37:07'),(681,50,'New task added to the project.','2021-08-09 05:15:15','2021-08-09 05:15:15'),(682,50,'New task added to the project.','2021-08-09 05:16:43','2021-08-09 05:16:43'),(683,50,'New task added to the project.','2021-08-09 05:26:55','2021-08-09 05:26:55'),(684,53,'Custom PM Tool added as new project.','2021-08-19 15:33:03','2021-08-19 15:33:03'),(685,53,'Akhtar is added as project member.','2021-08-19 15:33:18','2021-08-19 15:33:18'),(686,53,'New task added to the project.','2021-08-19 15:35:15','2021-08-19 15:35:15'),(687,50,'New task added to the project.','2021-08-19 17:15:53','2021-08-19 17:15:53'),(688,54,'Autoecuadmin-rebuild added as new project.','2021-08-19 17:40:00','2021-08-19 17:40:00'),(689,54,'Fahad is added as project member.','2021-08-19 17:40:55','2021-08-19 17:40:55'),(690,54,'Akhtar is added as project member.','2021-08-19 17:40:57','2021-08-19 17:40:57'),(691,50,'New task added to the project.','2021-08-20 18:19:09','2021-08-20 18:19:09'),(692,50,'New task added to the project.','2021-08-20 18:23:42','2021-08-20 18:23:42');
/*!40000 ALTER TABLE `project_activity` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:11:37
