-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'male',
  `locale` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `status` enum('active','deactive') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_remote` tinyint(1) DEFAULT NULL,
  `is_remote_check` tinyint(1) DEFAULT '0',
  `monthly_working_hours` int DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Hafiz Siddiq Umer','hafiz.siddiq.umer@bitsoftsol.com','$2y$10$QIQwLkyoUWAtD5Lt7se6NegPptpR5qTvB2.QkYtt/wuopCltNNjgq','G8dmnuqc2yb6ABgm7NfuE6iuckUVDoQ2GlTGixbgw3odTegbExjJrwxhGfip','Ps9m84DoIA75ROxxs6j9wck2sywoICDlq2Sm1cur.png',NULL,'male','en','active','2019-09-16 16:13:02','2021-07-16 03:51:18',NULL,0,196),(2,'Wali Shah','wali@bitsoftsol.com','$2y$10$.EBSc9EAs1iYUThfKd5MnuagrplsebDReDcj6YzO./3/.iiwVIMqe','DgrLKXTg8s5zd5FUSRSJjDWrQLqLpm8ySKH68OSWjxOQIA23ErADJaJTVd3W',NULL,'+92 336 0796074','male','en','deactive','2019-09-16 19:42:59','2020-02-07 03:18:50',NULL,0,0),(3,'Waheed','waheed@bitsoftsol.com','$2y$10$6ptZUrb65A7Z718NSCQGfuAtRwvKskSvf3OVGeDMSZWgQBCwDdt8u',NULL,'ydExQODMxMP7lQ0VYw5PyydIZmswxwaYMSTLSCqR.jpeg',NULL,'male','en','deactive','2019-09-16 19:45:51','2021-04-02 11:31:10',NULL,0,0),(5,'Mirza Ali','ali@bitsoftsol.com','$2y$10$7sd.26QsdvKE.7VLGK2CTObC7KZNxfVQwRFXQ45dE45bk2VQr3qOe','E596KjK7n3iowCv0UOzeJbJMwDBFlSpxh7eB6ZKhrOZIKxJOA4sutKo3rMW7','UvwRYZJSAQLAdY1L1KP4DdKwoG6A0ePqkowJAyED.jpeg','923069387974','male','en','deactive','2019-09-21 18:36:44','2021-04-02 11:31:57',NULL,0,0),(6,'Sheraz','sheraz@bitsoftsol.com','$2y$10$pnwVoMcHC3KxGXMYS3nQ2Od/o6mGCtMF7bl.5sthyh9qa8q89my4S','bRbp7nFEuke6FAWnG9iV6FttMA6mDfNgdvzJKOWv5bP6OBGfKKqUIlCMZXqo',NULL,'N/A','male','en','deactive','2019-10-04 17:43:55','2021-04-02 11:32:24',NULL,0,0),(7,'Ahsan Ansari','ahsan.ansari2@gmail.com','$2y$10$0vpNqIYEtu426V7yjN7YhuNVbHx08yHFcIIqGrD46duP1cEpCBGhi',NULL,NULL,NULL,'male','en','deactive','2019-10-12 17:15:23','2019-11-11 14:35:41',NULL,0,0),(8,'Aqib','aqib@bitsoftsol.com','$2y$10$laTuLaM/FJCnnNNpwtSMYuvhbkBuzDJyU.XiaBs3DDqWgLSqWMHRW','XVjqEMiZj5nCduuXahmCkyk0s8kL4vwMrt9EhxsEqPhwho8H21nqu4iTCD3U','SoCzYggg2BUGbLzOZ80Z51B6c6YdcHutZVxvBcdg.jpeg','03042499660','male','en','deactive','2019-11-05 17:55:15','2021-04-02 11:32:33',NULL,0,0),(10,'Rashid','rashid@bitsoftsol.com','$2y$10$/R5Z2vt.TrYCuJHIPj76suNnWtpc34UKPp8o91RQ2K3YsY2IKsY5u','hsjpCHE0B51B8nOhx5oxU3CtHKCUKK7b4zQeM6UbJoy0CKrKfcauPC4IphNh',NULL,NULL,'male','en','deactive','2019-11-28 17:50:12','2019-12-03 22:09:02',NULL,0,0),(11,'Asadullah','asad@bitsoftsol.com','$2y$10$BLL2edZnV4go1GE9srIGN.fOCtkulkBEJM85MKLaETCZqGrDlJwEe','3ohVi1oOwtIf18S2atnp8yVLEARjnWt6WNXUgxN1c1gWC3itDnzrpALuqYCT',NULL,'923126972242','male','en','deactive','2019-12-04 19:00:25','2020-02-07 03:21:34',NULL,0,0),(12,'Fahad','fahad.cs2k16@gmail.com','$2y$10$sxC9TfynRRxVv0ULTpvUHuAqVpshXF323Zwcm0Mwh6tEPwpt1mzVW','c13UZfF0ZWgTkFNR181XJlphAJyL8HS60tHbtlsAXrudzwsazI9Zf4aDEX5z',NULL,'923056641186','male','en','active','2019-12-09 16:10:32','2021-07-16 03:51:27',NULL,0,196),(14,'Naina yameen','shazma.yameen@bitsoftsol.com','$2y$10$q2k0OR8CHDHuZB4zF65ZgeMi56iwR/gazzBtQmS/3BTAL4hCgSQ9G','B6EEMro2lhJnNvtvY6K2xPyut5EAbGepeZ12QQ90I3PwuTOwVhNU2qoWEMtN','vZIUuq9ZDDUA5oLhN6drSGHjxu6CIOPWEqMjxD5q.jpeg','92','female','en','deactive','2020-01-12 19:08:27','2021-04-02 11:33:41',NULL,0,0),(15,'Farwa','farwa.siddiq76@gmail.com','$2y$10$0d2NmE7mvtAhrRZGVjIdV.sQhzjlDCdIhwuqNPGqFs3yZRGeeSKIG',NULL,NULL,'9211111111','female','en','deactive','2020-03-13 15:01:33','2021-04-02 11:33:45',NULL,0,0),(16,'muaviya','muaviya@bitsoftsol.com','$2y$10$Dws.T0GTCrQvLf74DGKg1u1T9hh/5MFHIlBLvN4hL6fwZefQ9Bn2S',NULL,'luqnfp3pWOPdgOeyfYaE5OgHNuONFhORbc5DMQL9.jpeg',NULL,'male','en','active','2020-03-21 15:04:35','2021-09-27 04:42:22',NULL,0,150),(17,'Usman','usman@bitsoftsol.com','$2y$10$m.Q5gx0I2eXVpL.1xLTPeeuSxliDIuEYKB.r5k10f0k0syf3U4ULO','f6GAjd7l9of9hDZ60F2ABsuqUj9JoQVvUFyJvBSGywnJRyhPLRQyF53XcQlr',NULL,'03066522195','male','en','deactive','2020-04-24 20:17:55','2021-04-02 11:34:00',NULL,0,0),(18,'Sameer','sameer@bitsoftsol.com','$2y$10$/Tj0gDYqYCfxeH9tTv2mJOSUZbphngO4P42hu4wN5WVqfEuEh1oQy','QLcMajfdMgT6eFJ8xgqEWnEOzQqLmZ35h4aWYtEJFKZrdJgl0aTQzSFB2w21',NULL,'92xxxxxx','male','en','deactive','2020-06-24 12:34:07','2021-04-02 11:34:06',NULL,0,0),(19,'Akhtar','akhtarm821@gmail.com','$2y$10$HTlh7lBKKAL0CmeAvGUaouCmRSCG.KC5bgFi9iO1OYJaEP1P5cJHq','TPbvLO0WqSCiqswbl1Mqp4PrnUVBa6VXE8qk4SKel23p3mZgilVJ4gDSMnjc','mEpNntO8no0l1QGowndu8Xt8gpRMoktuiph8FkEZ.jpeg','+923098638327','male','en','active','2020-06-24 12:35:46','2021-09-13 06:27:38',NULL,0,196),(20,'Moaviz','moaviz@bitsoftsol.com','$2y$10$3FFJwQvVz1Sp1ZqmeKgoPe.vKqdKlfii/CjHhVx658Ts4RE44zaKC','ZGGCpR0eQraPsJnbEyNbXebu5pz9wlxAe5fkPoktcZWKqTqc7814RVgg0Qyv',NULL,'923xxxxx','male','en','deactive','2020-06-24 12:41:43','2021-04-02 11:34:12',NULL,0,0),(21,'Mehwish','engr.mehwishahmed@gmail.com','$2y$10$OmXO5SEDuXe/g/I9jj9MQeaGVnE.z6g.imFm3MyoCcenKdaaxwG2m','rlqsDkspMyXdwCb3ZVb8isXKR9NDBTraRkJkMOEPPEggFxbi8LOpHqhsXwc8',NULL,'923000000000','female','en','deactive','2020-07-14 13:22:38','2021-09-13 06:30:17',NULL,1,196),(22,'Hamza','hamza786066@gmail.com','$2y$10$7gX1zEST1Rm/jvFy06OWP.jK4ZRYDH1eKDRwvUDekK.ylxQopV6i6','q7f5gGHmq2iJ7dmu6ygXjZHZuOe2Qe0vz3RYWfne5II4KFjR70Iw3mfXP778',NULL,'92xxxxxx','male','en','active','2021-02-03 16:18:24','2021-09-13 06:28:10',NULL,0,196),(23,'Kishan','expowebdeveloper@gmail.com','$2y$10$/VNB.tvxlbe6It2OPJvtLOdr8jyQGU40G1xgQJuYLTdF5k16Es2ea',NULL,NULL,NULL,'male','en','deactive','2021-05-31 06:34:06','2021-06-15 07:27:55',NULL,1,0),(25,'Ahmad Yar','ahmadyar228@gmail.com','$2y$10$dN8JwDmREx1TwdBbAvRU0uJg6Z6fDl8mLY0j3GBypnn31SSaeRzPO','iDno0gtUTwuzsKW1RnOkjoN7al8n75G6XnyJvAHKETYMZVjxJEVOFiPwljXY',NULL,'03065054079','male','en','deactive','2021-06-12 05:25:54','2021-07-31 04:52:37',NULL,1,0),(26,'Farhan','farhanfida405.ff@gmail.com','$2y$10$XQ8NHOBmFoY/m3ZVI5nYR.A0LNe7i15noze8NaeD9wJHR.4an/lX.','d9wG55jPjJgLd05MYgeUdUW4pF72ZqtNcHctBRi04R6R9DR1dfRMmPkm7bO6',NULL,'+92xxxxxxxxx','male','en','deactive','2021-08-02 05:56:00','2021-09-13 06:25:58',NULL,1,52),(27,'Waqas','Waqasnazar0786@gmail.Com','$2y$10$1A0c3nFIs12f1kaYWOBK.OJIHNtGcBsmqVaTGnNh4QQ3YC6JGKJTe',NULL,NULL,'+923047762669','male','en','active','2021-08-23 03:41:41','2021-08-23 03:41:41',NULL,0,196),(28,'Muhammad Asad','asad.seo@bitsoftsol.com','$2y$10$ZJN3PIEheY5coKlH5vQE2uHbSleN1H9pTq3LLCfp7zkX9GN6.CbzK','BfB5MWArpVW1BZVLye1zwGlT1eBALa1BoGQwsJjofn3iU8EX7QrhndcL7Fuw',NULL,'03xx','male','en','active','2021-08-25 05:39:21','2021-08-25 05:39:21',NULL,0,196),(29,'Babar','babar@bitsoftsol.com','$2y$10$8XiFwjbvJ6DiPAw1dIL0N.3NRS0FG/cDURYNy.UPFaVsPjItlcOXi','OrT1yZ1CN5GXnH5oP8Yiu4K1nCrTDTZ4iVPKD6gVZGe1yLm8HX2gEV4Z0ZsS',NULL,'+92xxxxx','male','en','active','2021-08-26 03:41:12','2021-08-26 03:41:12',NULL,0,196),(30,'Zaravar Janitor','jaravar@gmail.com','$2y$10$QabdbL4uNncrQRAy69tr9uZ7Q7AnYJFiBA4/vWx4lPeBOgPs73paa',NULL,NULL,'+923000xxxx','male','en','active','2021-09-13 06:31:26','2021-09-13 06:31:26',NULL,0,196),(31,'Jameel Roti Lene Wale','Jameel@gmail.com','$2y$10$xnW03Ile6Kyixz24FUnnful7U/NrNMCVF3kgXzisXYTOj3Zdl7die',NULL,NULL,'+92xxxxxxx','male','en','active','2021-09-13 06:33:16','2021-09-13 06:33:16',NULL,1,40);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:14:35
