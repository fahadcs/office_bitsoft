-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `projects` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `project_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `project_summary` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `project_admin` int unsigned DEFAULT NULL,
  `start_date` date NOT NULL,
  `deadline` date NOT NULL,
  `notes` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `category_id` int unsigned DEFAULT NULL,
  `client_id` int unsigned DEFAULT NULL,
  `feedback` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `manual_timelog` enum('enable','disable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'disable',
  `client_view_task` enum('enable','disable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'disable',
  `completion_percent` tinyint NOT NULL,
  `calculate_task_progress` enum('true','false') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_category_id_foreign` (`category_id`),
  KEY `projects_client_id_foreign` (`client_id`),
  KEY `projects_project_admin_foreign` (`project_admin`),
  CONSTRAINT `projects_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `project_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `projects_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `projects_project_admin_foreign` FOREIGN KEY (`project_admin`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (44,'Django Admin',NULL,NULL,'2021-05-31','2021-07-31',NULL,1,NULL,NULL,'disable','disable',0,'true','2021-05-31 06:33:09','2021-06-29 04:32:51'),(45,'Self-Bitsoft Creds',NULL,NULL,'2021-05-31','2021-07-10',NULL,1,NULL,NULL,'disable','disable',0,'true','2021-05-31 06:40:44','2021-05-31 06:40:44'),(46,'Tocino',NULL,NULL,'2021-05-31','2021-05-31',NULL,1,NULL,NULL,'disable','disable',0,'true','2021-05-31 06:43:37','2021-05-31 06:43:37'),(47,'Matrix Switch',NULL,NULL,'2021-06-15','2021-07-31',NULL,1,NULL,NULL,'disable','disable',0,'true','2021-06-17 04:37:34','2021-06-17 04:37:34'),(48,'Store Website Gaminglight',NULL,NULL,'2021-07-02','2021-07-10',NULL,1,NULL,NULL,'disable','disable',0,'true','2021-07-02 04:19:40','2021-07-02 04:19:40'),(49,'Home Work Hamza',NULL,NULL,'2021-08-01','2021-10-31',NULL,11,NULL,NULL,'enable','disable',0,'true','2021-07-27 10:46:03','2021-07-27 10:46:03'),(50,'jkp_portal',NULL,NULL,'2021-07-30','2021-08-15',NULL,1,NULL,NULL,'disable','disable',67,'true','2021-07-30 05:12:32','2021-08-22 17:08:22'),(51,'Backlinks KCK',NULL,NULL,'2021-08-01','2021-12-31',NULL,1,NULL,NULL,'enable','disable',0,'true','2021-08-02 06:00:40','2021-08-02 06:00:40'),(52,'Backlinks LDB',NULL,NULL,'2021-08-01','2021-12-31',NULL,1,NULL,NULL,'enable','disable',0,'true','2021-08-02 06:07:36','2021-08-03 05:37:07'),(53,'Custom PM tool',NULL,NULL,'2021-08-01','2021-08-31',NULL,1,NULL,NULL,'disable','disable',0,'true','2021-08-19 15:33:03','2021-08-19 15:33:03'),(54,'autoecuadmin-rebuild',NULL,NULL,'2021-08-01','2021-12-31',NULL,1,NULL,NULL,'disable','disable',0,'true','2021-08-19 17:40:00','2021-08-19 17:40:00');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:06:21
