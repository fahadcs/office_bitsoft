-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: self-db.cabwvixffofc.us-east-2.rds.amazonaws.com    Database: office_bitsoft
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `project_members`
--

DROP TABLE IF EXISTS `project_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_members` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `project_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_members_project_id_foreign` (`project_id`),
  KEY `project_members_user_id_foreign` (`user_id`),
  CONSTRAINT `project_members_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `project_members_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_members`
--

LOCK TABLES `project_members` WRITE;
/*!40000 ALTER TABLE `project_members` DISABLE KEYS */;
INSERT INTO `project_members` VALUES (80,1,44,'2021-05-31 06:39:10','2021-05-31 06:39:10'),(81,12,45,'2021-05-31 06:41:03','2021-05-31 06:41:03'),(82,19,45,'2021-05-31 06:41:04','2021-05-31 06:41:04'),(83,1,46,'2021-05-31 06:43:49','2021-05-31 06:43:49'),(84,25,46,'2021-06-12 05:27:52','2021-06-12 05:27:52'),(85,1,47,'2021-06-17 04:37:52','2021-06-17 04:37:52'),(86,25,47,'2021-06-17 04:37:55','2021-06-17 04:37:55'),(87,25,44,'2021-06-29 04:33:25','2021-06-29 04:33:25'),(88,1,48,'2021-07-02 04:19:54','2021-07-02 04:19:54'),(89,12,48,'2021-07-02 04:19:57','2021-07-02 04:19:57'),(90,22,49,'2021-07-27 10:46:17','2021-07-27 10:46:17'),(91,12,50,'2021-07-30 05:12:51','2021-07-30 05:12:51'),(92,19,50,'2021-07-30 05:12:54','2021-07-30 05:12:54'),(93,26,51,'2021-08-02 06:01:09','2021-08-02 06:01:09'),(94,26,52,'2021-08-02 06:08:04','2021-08-02 06:08:04'),(95,19,53,'2021-08-19 15:33:15','2021-08-19 15:33:15'),(96,12,54,'2021-08-19 17:40:49','2021-08-19 17:40:49'),(97,19,54,'2021-08-19 17:40:55','2021-08-19 17:40:55');
/*!40000 ALTER TABLE `project_members` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-13  3:08:36
