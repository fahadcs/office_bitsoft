@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')



<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/morrisjs/morris.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div role="alert" id="error-div">
                    
                            </div>
                            <div id="detail-div">
                            </div>
                            <form action="">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label style=" margin-top: 10px" for="date">Date</label>
                                        <input value="{{ $existing == null ? old('datepicker[]') : $existing->date }}"
                                            autocomplete="off" placeholder="Select Date"
                                            id="datepicker" name="datepicker[]" type="text" class="form-control datepicker"
                                            required="" />
                                    </div>
            
            
                                    <input type="hidden" id="currentRow" />
                                    <input type="hidden" id="current" />
            
            
                                    <div class="form-group col-md-12">
                                        <label for="vouchertype">Voucher Type</label>
                                        <select id="vouchertype" class="form-control " name="vouchertype">
                                            <option @if($existing==null) selected @endif value="Select One">Select One
                                            </option>
                                            <option @if($existing !=null && $existing->voucher_type == 'Cash Payment')
                                                selected @endif value="Cash Payment">Cash Payment</option>
                                            <option @if($existing !=null && $existing->voucher_type == 'Cash Receipt')
                                                selected @endif value="Cash Receipt">Cash Receipt</option>
                                            <option @if($existing !=null && $existing->voucher_type == 'Bank Payment')
                                                selected @endif value="Bank Payment">Bank Payment</option>
                                            <option @if($existing !=null && $existing->voucher_type == 'Bank Receipt')
                                                selected @endif value="Bank Receipt">Bank Receipt</option>
                                            <option @if($existing !=null && $existing->voucher_type == 'Journal Voucher')
                                                selected @endif value="Journal Voucher">Journal Voucher</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group">
                                        <label for="accountname">Account Name</label>
                                        <select id="accountname"
                                            class="form-control js-example-basic-single" name="accountname">
                                            <option value="Select One">Select One</option>
                                            @foreach ($accountname as $item)
                                            <option value="{{$item->id}}">{{$item->account_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group">
                                        <label for="narration">Narration</label>
                                        <textarea name="narration" id="narration" placeholder="Enter Narration" class="form-control" rows="3" id="comment"
                                            required></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group">
                                        <label for="vouchertype">Amount</label>
                                        <input placeholder="Enter Amount" id="amount" name="amount"
                                            type="number" class="form-control" required>
                                    </div>
                                </div>
                                
                                <input type="button" value="Credit" id="credit" name="credit"
                                    class="btn btn-warning pull-right">
                                <input type="button" value="Debit" id="debit" name="debit" class="btn btn-primary pull-right"
                                    style="margin-right: 10pt">

                            </form>

                        </div>
                        <div class="col-md-8">
                            <label for="vouchertype">Voucher Information Data</label>
                            <form action="">
                                <table id="voucherdata" class="table table-bordered">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Account Name</th>
                                            <th scope="col">Narration</th>
                                            <th scope="col">Debit</th>
                                            <th scope="col">Credit</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="voucher">
                                        @isset($detailexist)
                                        @if(count($detailexist) > 0)
                                        @foreach ($detailexist as $item)
                                        <tr id="row-{{$item->vid}}">
                                            <td>{{ $item->account_name }}</td>
                                            <td>{{ $item->narration }}</td>
                                            <td>{{ $item->debit }}</td>
                                            <td>{{ $item->credit }}</td>
                                            <td><input type='button' data-accid={{ $item->accountid }}
                                                    data-id={{ $item->vid }} id='edit' value='Edit'
                                                    class='btn btn-primary btn-sm'><input type='button'
                                                    data-accid={{ $item->accountid }} data-id={{ $item->vid }} id='delt'
                                                    value='Delete' class='btn btn-danger btn-sm'></td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        <input type="hidden" id="editset" value="editset">
                                        @endisset
                                    </tbody>
                                </table>
                            </form>
                            <button type="submit" name="savedata" id="savedata"
                                class="btn btn-danger pull-right">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    


    <div class="col-md-12">
        <div class="white-box">

            <div class="table-responsive">
                <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="tblVoucherDetails">
                    <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Date
                        </th>
                        <th>
                            Voucher Type
                        </th>
                        <th>
                            Credit Sum
                        </th>
                        <th>
                            Debit Sum
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<!------------------  -->







<div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Details of Vouchers Record</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="voucher-modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="vMasterId">

<input type="hidden" id="delete_id">
<input type="hidden" id="voucher_detail_id">

@endsection
@push('footer-script')


<script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
<script src=" {{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    $('.datepicker').datepicker({
format: 'yyyy-mm-dd',
autoclose: true,
todayHighlight: true,
});
function initDatepicker() {
$('.wrapper').find('.datepicker').each(function(i, e) {
$(this).removeClass('hasDatepicker').datepicker({
  format: 'yyyy-mm-dd',
  autoclose: true,
  todayHighlight: true,
});
});
}
</script>
<script>
    function Confirm(id) {
            $('#delete_id').val(id);
            $('#delete_model').modal('show');
        }
    function showDetails(id)
    {
    //$('#delete_id').val(id);
    //$('#delete_model').modal('show');
    var url = "{{route('accounts_manager.vouchers.showDetail')}}";
            data = {
            "_token": "{{ csrf_token() }}",
            "voucher_master_id":id,
            }
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (response) {

                    if (response['success'] == true) {
                        //swal("Add Data and try again.")
                        //$('#detail-modal').modal('show');
                        var swal_html = '<table id="masterdetail" style="" class="table table-bordered"><thead class="thead-dark"><tr><th scope="col">Master ID</th><th scope="col">Date</th><th scope="col">Voucher Type</th><th scope="col">CreditSum</th><th scope="col">DebitSum</th></tr></thead><tbody id="masterdetailbody">';
                            var credsum;
                            var debsum;
                            var dt;
                            var masterid;
                            var accounttype;
                            credsum=response['voucher_master']['credit_sum'];

                            debsum=response['voucher_master']['debit_sum'];

                            dt=response['voucher_master']['date'];
                            masterid=response['voucher_master']['id'];

                            accounttype=response['voucher_master']['voucher_type'];

                            swal_html +='<tr><td>'+masterid+'</td><td>'+dt+'</td><td>'+accounttype+'</td><td>'+credsum+'</td><td>'+debsum+'</td></tr>';

                            swal_html +='</tbody></table>';
                        swal_html += '<table id="detaildata" style="" class="table table-bordered"><thead class="thead-dark"><tr><th scope="col">Account Name</th><th scope="col">Narration</th><th scope="col">Debit</th><th scope="col">Credit</th></tr></thead><tbody id="detailbody">';


                        $.each(response['record'], function(key, record) {

                           swal_html +='<tr><td>'+record.account_name+'</td><td>'+record.narration +'</td><td>'+record.debit +'</td><td>'+record.credit+'</td></tr>';

                        });
                        swal_html +='</tbody></table>';
                        //alert(swal_html);
                        //swal({title:"Detail", html: swal_html});
                        $("#voucher-modal-body").html(swal_html);
                        $('#detail-modal').modal('show');
                        return;
                    }
                    //swal("Data Successfully Saved");
                    //window.location.reload();
                },
                error: function(error) {
                    let response = error['responseText'];
                    alert("There is an Error try Again:" + response);
                }
            });
}




$(document).ready(function () {
    $('.nav li.active').removeClass('active');
    $('#voucher').addClass('active');
    $('.js-example-basic-single').select2();/////Select with search box

            var rowCount = $('#voucherdata tr').length;
            if(rowCount>1)
            {
                total();
            }

            $('#tblVoucherDetails').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{route('accounts_manager.vouchers.data')}}",
                'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                        if(aData['debit_sum']!=aData['credit_sum']) {
                            $('td', nRow).css('background-color', 'hsl(0, 100%, 85%)');
                        }
                        else
                        {
                            $('td', nRow).css('background-color', '#9EFF98');
                        }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'date', name: 'date'},
                    {data: 'voucher_type', name: 'voucher_type'},
                    {data: 'debit_sum', name: 'debit_sum'},
                    {data: 'credit_sum', name: 'credit_sum'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });



        $(document).on('click', '.delete_voucher',function (e) {
           var id = $(this).data('id');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('accounts_manager.vouchers.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                /* $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw(); */

                                window.location.reload();
                            }
                        }
                    });
                }
            });
        });

    var v_d_id='';

        $("#savedata").click(function(){
            var url = "{{route('accounts_manager.vouchers.saveform')}}";
            data = {
            "_token": "{{ csrf_token() }}",
            }
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (response) {

                    if (response['success'] == false) {
                        swal("Add Data and try again.")
                        return;
                    }
                    swal("Data Successfully Saved");
                    window.location.href = "{{route('accounts_manager.vouchers.index')}}";

                },
                error: function(error) {
                    let response = error['responseText'];
                    alert("There is an Error try Again:" + response);
                }
            });
        });



    var accountid='';
    accountid=$("#accountname").val();
        $("#credit").click(function(){
            var datepicker=$("#datepicker").val();
            var vouchertype=$("#vouchertype").val();
            var accountname=$("#accountname option:selected").text();
            accountid=$("#accountname").val();
            var narration=$("#narration").val();
            var credit=$("#amount").val();
            var voucher_detail_id=$("#voucher_detail_id").val();
            var debit=0;
            if (!$.trim($("#datepicker").val())) {
                swal("Empty!", "Enter Valid Date");
                return;
            }
            if (vouchertype==="Select One"){
                swal("Empty!", "Select Voucher Type");
                return;
            }
            if (accountname==="Select One"){
                swal("Empty!", "Select Account Name");
                return;
            }
            if (!$.trim($("#narration").val())) {
                // textarea is empty or contains only white-space
                swal("Empty!", "Add Narration");
                return;
            }
            if (!$.trim($("#amount").val())) {
                swal("Empty!", "Enter Amount");
                return;
            }

            var url = "{{route('accounts_manager.vouchers.store')}}";
            data = {
            "_token": "{{ csrf_token() }}",
            "date":datepicker,
            "voucher_type":vouchertype,
            "account_name":accountname,
            "account_id":accountid,
            "narration": narration,
            "debit":debit,
            "credit":credit,
            "voucher_detail_id":voucher_detail_id,
            }
            $.ajax({
                type: "post",
                url: url,
                data: data,
                // dataType: "dataType",
                success: function (response) {
                    console.log(response);

                    if (response['success'] == false) {
                        swal("There Are errors, please fix and try again.")
                        let error_html = '<div class="alert alert-warning alert-dismissible fade show"><ul>';
                        $.each(response['errors'], function(key, value) {
                            error_html += "<li>" + value + "</li>";
                        });
                        error_html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></ul></div>';
                        $("#error-div").html(error_html);
                        return;
                    }
                    var voucher_detail = response['voucher_detail'];
                    v_d_id=voucher_detail['id'];
                    //voucher_detail_id=voucher_detail['id'];
                    //alert(voucher_detail['id']);
                    var rows="<tr id='row-"+voucher_detail['id']+"'><td data-id="+voucher_detail['id']+">"+accountname+"</td><td>"+narration+"</td><td>"+debit+"</td><td>"+credit+"</td>"
                 +"<td><input type='button' data-id="+voucher_detail['id']+" id='edit' value='Edit' class='btn btn-primary btn-sm'><input type='button' data-id="+voucher_detail['id']+" id='delt' value='Delete' class='btn btn-danger btn-sm'></td>"+"</tr>";

                var currentRow=$("#currentRow").val();

            if(currentRow){
               // alert(currentRow);
                var i = currentRow;
                if(parseInt(i)===0)
                {
                    $('#voucherdata > tbody > tr').eq(parseInt(i)).before(rows);

                }
                else
                {
                    $('#voucherdata > tbody > tr').eq(parseInt(i)-1).after(rows);
                }
                $("#voucherdata > tbody > tr").eq(parseInt(i)+1).remove();
                $("#currentRow").val("");
            }
            else
            {
                //$(rows).appendTo("#Voucher tbody");
                $('#voucherdata > tbody').append(rows);
            }


            total();
            //Clear the Fields
            $('#accountname').prop('selectedIndex',0);//combo select first item
            $('#accountname').select2().trigger('change');///For Select2
            $('#narration').val("");
            $('#amount').val("");
                    //alert("Data Saved");
                    //Add data to table
                },
                error: function(error) {
                    let response = error['responseText'];
                    alert("There is an Error:" + response);
                }
            });




        });

        $("#debit").click(function(){
            var datepicker=$("#datepicker").val();
            var vouchertype=$("#vouchertype").val();
            var accountname=$("#accountname option:selected").text();
             accountid=$("#accountname").val();
            var narration=$("#narration").val();
            var debit=$("#amount").val();
            var voucher_detail_id=$("#voucher_detail_id").val();
            var credit=0;

            if (!$.trim($("#datepicker").val())) {
                swal("Empty!", "Enter Valid Date");
                return;
            }

            if (vouchertype==="Select One"){
                swal("Empty!", "Select Voucher Type");
                return;
            }
            if (accountname==="Select One"){
                swal("Empty!", "Select Account Name");
                return;
            }
            if (!$.trim($("#narration").val())) {
                // textarea is empty or contains only white-space
                swal("Empty!", "Add Narration");
                return;
            }
            if (!$.trim($("#amount").val())) {
                swal("Empty!", "Enter Amount");
                return;
            }
            /////////var starttime = date ("Y-m-d", strtotime(datepicker));
            /////////alert(starttime);
            var url = "{{route('accounts_manager.vouchers.store')}}";
            data = {
            "_token": "{{ csrf_token() }}",
            "date":datepicker,
            "voucher_type":vouchertype,
            "account_name":accountname,
            "account_id":accountid,
            "narration": narration,
            "debit":debit,
            "credit":credit,
            "voucher_detail_id":voucher_detail_id,
            }
            $.ajax({
                type: "post",
                url: url,
                data: data,
                // dataType: "dataType",
                success: function (response) {
                    //console.log(response);

                    if (response['success'] == false) {
                        swal("There Are errors, please fix and try again.")
                        let error_html = '<div class="alert alert-warning alert-dismissible fade show"><ul>';
                        $.each(response['errors'], function(key, value) {
                            error_html += "<li>" + value + "</li>";
                        });
                        error_html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></ul></div>';
                        $("#error-div").html(error_html);
                        return;
                    }
                    var voucher_detail = response['voucher_detail'];
                    //alert(voucher_detail['id']);
                    v_d_id=voucher_detail['id'];
                    //voucher_detail_id=voucher_detail['id'];
                    var rows="<tr id='row-"+voucher_detail['id']+"'><td>"+accountname+"</td><td>"+narration+"</td><td>"+debit+"</td><td>"+credit+"</td>"
                 +"<td><input type='button' data-id="+voucher_detail['id']+" id='edit' value='Edit' class='btn btn-primary btn-sm'><input type='button' data-id="+voucher_detail['id']+" id='delt' value='Delete' class='btn btn-danger btn-sm'></td>"+"</tr>";

                var currentRow=$("#currentRow").val();
                //alert(currentRow);
                //alert(v_d_id);

            if(currentRow){
                var i = currentRow;
                if(parseInt(i)===0)
                {
                    $('#voucherdata > tbody > tr').eq(parseInt(i)).before(rows);

                }
                else
                {
                    $('#voucherdata > tbody > tr').eq(parseInt(i)-1).after(rows);
                }
                $("#voucherdata > tbody > tr").eq(parseInt(i)+1).remove();
                $("#currentRow").val("");
            }
            else
            {
                //$(rows).appendTo("#Voucher tbody");
                $('#voucherdata > tbody').append(rows);
            }

            total();

            //Clear the Fields
            $('#accountname').prop('selectedIndex',0);//combo select first item
            $('#accountname').select2().trigger('change');///For Select2
            $('#narration').val("");
            $('#amount').val("");
                    //alert("Data Saved");
                    //Add data to table
                },
                error: function(error) {
                    let response = error['responseText'];
                    alert("There is an Error:" + response);
                }
            });
        });

    function total(){
        var rowCount = $('#voucherdata tr').length;
        var i='';
        if(rowCount!==0)
        {
            $('tr#tot').remove();//Sir's Code
            i = $("#tot").index();

            var creditSum=0;
            var debitSum=0;
            var credit=0;
            var debit=0;
            $('#voucherdata tbody tr').each(function () {
                debit      = parseInt($(this).find('td:eq(2)').text());
                credit = parseInt($(this).find('td:eq(3)').text());
                //console.log(debit);
                    creditSum = parseInt(creditSum) + parseInt(credit);
                    debitSum = parseInt(debitSum) + parseInt(debit);

            });
           // alert(debitSum+" "+creditSum);


            var rows="<tr id='tot'><th colspan='2'>Total</th><th>"+debitSum+"</th><th>"+creditSum+"</th><td></td></tr>";
            $('#voucherdata > tbody').append(rows);
        }
    }

    //Edit Table row button function
    $('#voucherdata tbody').on('click', '#edit', function(){

        v_d_id=$(this).data("id");
        $("#voucher_detail_id").val(v_d_id);
        //alert(v_d_id);
        var r = $(this).closest('tr');
        var rr = $(this).closest('tr').index();
        var accountname;

        if( $('#editset').length )
        {
            accountname=$(this).data("accid");
        }
        else
        {
            accountname = accountid;
        }






        var narration = r.find('td:eq(1)').text();
        var debit = r.find('td:eq(2)').text();
        var credit = r.find('td:eq(3)').text();
        var amount=credit;
        if(credit==='0')
        {
          var amount=debit;
        }
        //var name = "option[text=" + accountname + "]";
        $("#accountname").val(accountname);
        $('#accountname').select2().trigger('change');///For Select2
        $("#narration").val(narration);
        $("#amount").val(amount);
        //alert(r);
        //alert(rr);
        $("#currentRow").val(rr);
        $("#current").val(r);

    });


    //delete table row button works here
    $('#voucherdata tbody').on('click', '#delt', function(){

            v_d_id=$(this).data("id");
            swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        },
        function(willDelete) {
        if (willDelete) {
            ////////
            data = {
            "_token": "{{ csrf_token() }}",

            "voucher_detail_id":v_d_id,
            }
            var url = "{{ route('accounts_manager.accounts.delete-data') }}";

            $.ajax({
                type: "post",
                url: url,
                data:data,


            });
                $('#row-'+v_d_id).remove();
                total();
            ///////////////
            swal("Data Successfully Deleted!", {
            icon: "success",
            });
        }
        else
        {
            swal("Record is safe!");
        }
        });

      

    });

});


    /*
    function addData(){
        //var date=$("#datepicker").val();
        //var vouchertype=$("#vouchertype").val();
        var accountname=$("#accountname").val();
        var narration=$("#narration").val();
        var amount=$("#amount").val();
        var rows="";
        rows+="<tr><td>"+accountname+"</td><td>"+narration+"</td><td>"+0+"</td><td>"+amount+"</td></tr>";
        $(rows).appendTo("#Voucher tbody");
    }*/
</script>
@endpush
