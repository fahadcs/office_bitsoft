@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')



<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/morrisjs/morris.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['id'=>'accounts-container','class'=>'ajax-form','method'=>'POST']) !!}
                           
                            <div class="row">
                                <input type="hidden" value="{{ $existing == null ? old('id') : $existing->id }}"
                                    name="id">
                                <div class="col-md-4">
                                    <div
                                        class="form-group @if($errors->has('account_name')) has-danger bmd-form-group @endif">
                                        <label class="bmd-label-floating">Account Name</label>
                                        <input type="text" class="form-control"
                                            value="{{ $existing == null ? old('account_name') : $existing->account_name }}"
                                            name="account_name">
                                        @if ($errors->has('account_name'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('account_name') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div
                                        class="form-group @if($errors->has('account_type')) has-danger bmd-form-group @endif">
                                        <label class="bmd-label-floating">Select Account Type</label>
                                        <select class="form-control" name="account_type">
                                            <option @if($existing==null) selected @endif value="0"> -- Select Account
                                                Type -- </option>
                                            <option @if(old('account_type')=='Assets' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Assets') selected @endif
                                                value="Assets">Assets</option>
                                            <option @if(old('account_type')=='Liability' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Liability') selected @endif
                                                value="Liability">Liability</option>
                                            <option @if(old('account_type')=='Expense' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Expense') selected @endif
                                                value="Expense">Expense</option>
                                            <option @if(old('account_type')=='Capital' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Capital') selected @endif
                                                value="Capital">Capital</option>
                                            <option @if(old('account_type')=='Revenue' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Revenue') selected @endif
                                                value="Revenue">Revenue</option>
                                        </select>
                                        @if ($errors->has('account_type'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('account_type') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div
                                        class="form-group @if($errors->has('parent_id')) has-danger bmd-form-group @endif">
                                        <label class="bmd-label-floating">Select Parent Account</label>
                                        <select class="form-control" name="parent_id">
                                            <option @if(count($accounts)==0) selected @endif value="0"> -- Select Parent
                                                Account -- </option>
                                            @foreach($accounts as $account)
                                            <option value="{{ $account->id }}" @if($existing !=null && $existing->
                                                parent_id == $account->id) selected @endif>
                                                {{ $account->account_name }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('parent_id'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('parent_id') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div
                                        class="form-check @if($errors->has('is_parent')) has-danger bmd-form-group @endif">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="is_parent"
                                                @if($existing !=null && $existing->is_parent == true) checked @endif >
                                            Is Parent ?
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                        @if ($errors->has('is_parent'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('is_parent') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-danger pull-right save-account">Save</button>
                            <div class="clearfix"></div>
                            {!! Form::close() !!}
        </div>

        <div class="col-md-12">
            <div class="white-box">
                <div class="card-header card-header-danger">
                    <h4 class="card-title ">Chart Of Account Information</h4>
                    <p class="card-category"> Here All Chart Of Account Information Available</p>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="accounts-table">
                        <thead>
                            <th>
                                Account Name
                            </th>
                            <th>
                                Account Type
                            </th>
                            <th>
                                Is Parent ?
                            </th>
                            <th>
                                Parent Account
                            </th>
                            <th>
                                Action
                            </th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')


<script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
<script>
    $(function() {

        
            var table = $('#accounts-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: '{!! route('accounts_manager.accounts.data') !!}',
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                    {data: 'account_name', name: 'account_name'},
                    {data: 'account_type', name: 'account_type'},
                    {data: 'is_parent', name: 'is_parent'},
                    {data: 'parent_name', name: 'parent_name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
    

         
    $('.save-account').click(function () {
        $.easyAjax({
            url: '{{route('accounts_manager.accounts.store')}}',
            container: '#accounts-container',
            type: "POST",
            redirect: true,
            data: $('#accounts-container').serialize(),
            success: function (data) {
                if($.isEmptyObject(data.error)){

                    table._fnDraw();

                    }else{
                        printErrorMsg(data.error);
                        

                    }
            }
        })
    });

        function printErrorMsg (msg) {
        
                $.each( msg, function( key, value ) {
                    alert(value);
                });
            }
        
            $('body').on('click', '.delete-account', function(){
            var id = $(this).data('account-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('accounts_manager.accounts.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });


    });

   
    
       
       
       
</script>
@endpush
