@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
@endpush

@section('content')

    <div class="row">
        

        <div class="col-md-12">
            <div class="white-box">
               
              <div class="row">
                    <form action="">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="accountname">Account Name</label>
                                    <select id="accountname" class="form-control js-example-basic-single"
                                        name="accountname ">
                                        <option value="0">Select One</option>
                                        @foreach ($accountname as $item)
                                        <option value="{{$item->id}}">{{$item->account_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="date">StartDate</label>
                                    <input autocomplete="off"
                                        placeholder="Select Date" id="startDate" name="datepicker[]" type="text"
                                        class="form-control datepicker" required="" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="date">EndDate</label>
                                    <input autocomplete="off"
                                        placeholder="Select Date" id="endDate" name="datepicker[]" type="text"
                                        class="form-control datepicker" required="" />
                                </div>
                            </div>
                        </form>

                </div>
                
               

                <div class="table-responsive">
                 <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="tblLedgerDetails">
                        <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                Voucher Type
                            </th>
                            <th>
                                Account Name
                            </th>
                            <th>
                                Narration
                            </th>
                            <th>
                                Debit
                            </th>
                            <th>
                                Credit
                            </th>
                            <th>
                                Balance
                            </th>
                        </tr>
                        </thead>
                    </table> 
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></scrip
<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script src=" {{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script>
    function populateDatatable() {
        //Access Dropdown
        //Access Start Date
        //Access End Date
        //Server Conditions:
        //if all is none: Show All Data
        //If account is selected and dates are not, Then show data for that accuont
        //If Dates are selected without account then show all transactions within date
        //If all are selected then show filtered Data:
        var account =$('#accountname :selected').val();
        var startDate =$('#startDate').val();
        var endDate =$('#endDate').val();


        var table =$('#tblLedgerDetails').DataTable({
            processing: true,
            serverSide: true,
            ajax:{ type: 'get', url: "{{route('accounts_manager.ledger.data')}}",
            data: { "_token": "{{ csrf_token() }}", "account":account, "startDate":startDate, "endDate":endDate }},
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'date', name: 'date'},
                    {data: 'voucher_type', name: 'voucher_type'},
                    {data: 'account_name', name: 'account_name'},
                    {data: 'narration', name: 'narration'},
                    {data: 'debit', name: 'debit'},
                    {data: 'credit', name: 'credit'},
                    {data: 'balance', name: 'balance'},
                ]
            });
    }
    function checkDate(){
        $("#startDate").datepicker();
        $("#endDate").datepicker();
        var startDate =$('#startDate').val();
        var endDate =$('#endDate').val();
            if(startDate!=="" && endDate!=="")
                {
                    if(startDate<endDate)
                    {
                        $('#tblLedgerDetails').DataTable().clear().destroy();
                        populateDatatable();
                    }
                }
            else
            {
                $('#tblLedgerDetails').DataTable().clear().destroy();
                populateDatatable();
            }
    }

    $(document).ready(function () {

            $('.nav li.active').removeClass('active');
            $('#ledger').addClass('active');
            populateDatatable();
            $('.js-example-basic-single').select2();/////Select with search box
            $('select').on('change', function() {

                checkDate();
            });




            $("#startDate").datepicker();
            $("#endDate").datepicker();
            $("#startDate").on("change",function(){
                checkDate();
            });
            $("#endDate").on("change",function(){
                checkDate();
            });
});
</script>
{{-- <script>
    $(function() {



        var startDate = '{{ $startDate->format('Y-m-d') }}';
        var endDate = '{{ $endDate->format('Y-m-d') }}';

        $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        cancelClass: 'btn-inverse',
        "locale": {
            "applyLabel": "{{ __('app.apply') }}",
            "cancelLabel": "{{ __('app.cancel') }}",
            "daysOfWeek": [
                "{{ __('app.su') }}",
                "{{ __('app.mo') }}",
                "{{ __('app.tu') }}",
                "{{ __('app.we') }}",
                "{{ __('app.th') }}",
                "{{ __('app.fr') }}",
                "{{ __('app.sa') }}"
            ],
            "monthNames": [
                "{{ __('app.january') }}",
                "{{ __('app.february') }}",
                "{{ __('app.march') }}",
                "{{ __('app.april') }}",
                "{{ __('app.may') }}",
                "{{ __('app.june') }}",
                "{{ __('app.july') }}",
                "{{ __('app.august') }}",
                "{{ __('app.september') }}",
                "{{ __('app.october') }}",
                "{{ __('app.november') }}",
                "{{ __('app.december') }}",
            ]
        }
    })

    $('.input-daterange-datepicker').on('apply.daterangepicker', function (ev, picker) {
        startDate = picker.startDate.format('YYYY-MM-DD');
        endDate = picker.endDate.format('YYYY-MM-DD');
        console.log(startDate);
        console.log(endDate);
        showData();
        totalPaid();
    });

        $('#user_id').change(function () {
            
            showData();
            totalPaid();
        
        });


        function totalPaid(){
            userId = $("#user_id").val();
            var url = '{!!  route('admin.workPayment.total_paid', [':startDate', ':endDate', ':userId']) !!}';
            url = url.replace(':startDate', startDate);
            url = url.replace(':endDate', endDate);
            url = url.replace(':userId', userId);
        

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                
                $('#total_paid').html(response.total_paid);
            }
        });
        }

        showData();
        totalPaid();

        function showData() {

            
            userId = $("#user_id").val();
            var searchQuery = "?userId="+userId+"&start_date="+startDate+"&end_date="+endDate;
            var table = $('#workPayment-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            
            ajax: '{!! route('admin.workPayment.data') !!}'+searchQuery,
            "order": [[ 0, "desc" ]],
            deferRender: true,
            language: {
                "url": "#"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'date_range', name: 'date_range' },
                { data: 'payable', name: 'payable'},
                { data: 'payment', name: 'payment'},
                { data: 'bonus', name: 'bonus' },
                { data: 'balance', name: 'balance'},
                { data: 'pay_date', name: 'pay_date'},
                { data: 'action', name: 'action'}
            ]
        });

        }

        

        $('body').on('click', '.delete_workPayment', function(){
            var id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('admin.workPayment.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                totalPaid();
                                showData();
                            }
                        }
                    });
                }
            });
        });

    
    });    
    
</script> --}}
@endpush