@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
@endpush

@section('content')

    <div class="row">
        

        <div class="col-md-12">
            <div class="white-box">
               
              <div class="row">
                <form action="">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="accountname">Account Name</label>
                            <select id="accountname" class="form-control js-example-basic-single"
                                name="accountname ">
                                <option value="0">Select One</option>
                                @foreach ($accountname as $item)
                                <option value="{{$item->id}}">{{$item->account_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="date">StartDate</label>
                            <input autocomplete="off"
                                placeholder="Select Date" id="startDate" name="datepicker[]" type="text"
                                class="form-control datepicker" required="" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="date">EndDate</label>
                            <input autocomplete="off"
                                placeholder="Select Date" id="endDate" name="datepicker[]" type="text"
                                class="form-control datepicker" required="" />
                        </div>

                    </div>
                </form>

                </div>
                
               

                <div class="table-responsive">
                 <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="tblCompactLedgerDetails">
                        <thead>
                        <tr>
                            <th>
                                Account Name
                            </th>

                            <th>
                                Debit
                            </th>
                            <th>
                                Credit
                            </th>
                        </tr>
                        </thead>
                    </table> 
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>



<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script src=" {{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script>
    function populateDatatable() {

        var account =$('#accountname :selected').val();
        var length = $('#accountname > option').length;
        var startDate =$('#startDate').val();
        var endDate =$('#endDate').val();
        //For checking if there is any other option except "Select One"
        if(length>1)
        {
            var table =$('#tblCompactLedgerDetails').DataTable({
            processing: true,
            serverSide: true,
            ajax:{ type: 'get', url: "{{ route('accounts_manager.compactledger.data') }}",
            data: { "_token": "{{ csrf_token() }}", "account":account, "startDate":startDate, "endDate":endDate }},
                columns: [
                    {data: 'account_name', name: 'account_name'},
                    {data: 'debit_sum', name: 'debit_sum'},
                    {data: 'credit_sum', name: 'credit_sum'},
                ]
            });
        }

    }

    function checkDate(){
        $("#startDate").datepicker();
        $("#endDate").datepicker();
        var startDate =$('#startDate').val();
        var endDate =$('#endDate').val();

            if(startDate!=="" && endDate!=="")
                {
                    if(startDate<endDate)
                    {
                        $('#tblCompactLedgerDetails').DataTable().clear().destroy();
                        populateDatatable();
                    }
                }
                else if(startDate==="" && endDate!==""){
                    return;
                }
                else if(startDate!=="" && endDate===""){
                    return;
                }
            else
            {
                $('#tblCompactLedgerDetails').DataTable().clear().destroy();
                populateDatatable();
            }
    }
    $(document).ready(function () {

            $('.nav li.active').removeClass('active');
            $('#compact_ledger').addClass('active');
            populateDatatable();
            $('.js-example-basic-single').select2();/////Select with search box
            $('select').on('change', function() {

                checkDate();
            });
            $("#startDate").datepicker();
            $("#endDate").datepicker();
            $("#startDate").on("change",function(){
                checkDate();
            });
            $("#endDate").on("change",function(){

                checkDate();
            });
});
</script>

@endpush