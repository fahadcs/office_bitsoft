@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
@endpush

@section('content')

    <div class="row">
        

        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route('admin.workPayment.create') }}" class="btn btn-outline btn-success btn-sm">Add New Work Payment<i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>


                    
                    <div class="col-sm-6 text-right hidden-xs">
                        <div class="form-group">
                            <a href="{{ route('admin.employees.export') }}" class="btn btn-info btn-sm"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.timeLogs.employeeName')</label>
                            <select class="select2 form-control" data-placeholder="Choose Employee" id="user_id" name="user_id">
                                <option value="0">All</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">@lang('app.selectDateRange')</label>
    
                        <div class="form-group">
                            <input class="form-control input-daterange-datepicker" type="text" name="daterange"
                                   value="{{ $startDate->format('m/d/Y').' - '.$endDate->format('m/d/Y') }}"/>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="white-box bg-success">
                            <h3 class="box-title text-white">Total Paid</h3>
                            <ul class="list-inline two-part">
                                <li><i class="icon-clock text-white"></i></li>
                                <li class="text-right"><span id="total_paid" class="counter text-white"></span></li>
                            </ul>
                        </div>
                    </div>
                </div>

               

                <div class="table-responsive">
                 <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="workPayment-table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date Range</th>
                            <th>Pyable</th>
                            <th>Paid</th>
                            <th>Bonus</th>
                            <th>Balance</th>
                            <th>Pay Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table> 
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
    $(function() {



        var startDate = '{{ $startDate->format('Y-m-d') }}';
        var endDate = '{{ $endDate->format('Y-m-d') }}';

        $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        cancelClass: 'btn-inverse',
        "locale": {
            "applyLabel": "{{ __('app.apply') }}",
            "cancelLabel": "{{ __('app.cancel') }}",
            "daysOfWeek": [
                "{{ __('app.su') }}",
                "{{ __('app.mo') }}",
                "{{ __('app.tu') }}",
                "{{ __('app.we') }}",
                "{{ __('app.th') }}",
                "{{ __('app.fr') }}",
                "{{ __('app.sa') }}"
            ],
            "monthNames": [
                "{{ __('app.january') }}",
                "{{ __('app.february') }}",
                "{{ __('app.march') }}",
                "{{ __('app.april') }}",
                "{{ __('app.may') }}",
                "{{ __('app.june') }}",
                "{{ __('app.july') }}",
                "{{ __('app.august') }}",
                "{{ __('app.september') }}",
                "{{ __('app.october') }}",
                "{{ __('app.november') }}",
                "{{ __('app.december') }}",
            ]
        }
    })

    $('.input-daterange-datepicker').on('apply.daterangepicker', function (ev, picker) {
        startDate = picker.startDate.format('YYYY-MM-DD');
        endDate = picker.endDate.format('YYYY-MM-DD');
        console.log(startDate);
        console.log(endDate);
        showData();
        totalPaid();
    });

        $('#user_id').change(function () {
            
            showData();
            totalPaid();
        
        });


        function totalPaid(){
            userId = $("#user_id").val();
            var url = '{!!  route('admin.workPayment.total_paid', [':startDate', ':endDate', ':userId']) !!}';
            url = url.replace(':startDate', startDate);
            url = url.replace(':endDate', endDate);
            url = url.replace(':userId', userId);
        

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                
                $('#total_paid').html(response.total_paid);
            }
        });
        }

        showData();
        totalPaid();

        function showData() {

            
            userId = $("#user_id").val();
            var searchQuery = "?userId="+userId+"&start_date="+startDate+"&end_date="+endDate;
            var table = $('#workPayment-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            
            ajax: '{!! route('admin.workPayment.data') !!}'+searchQuery,
            "order": [[ 0, "desc" ]],
            deferRender: true,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'date_range', name: 'date_range' },
                { data: 'payable', name: 'payable'},
                { data: 'payment', name: 'payment'},
                { data: 'bonus', name: 'bonus' },
                { data: 'balance', name: 'balance'},
                { data: 'pay_date', name: 'pay_date'},
                { data: 'action', name: 'action'}
            ]
        });

        }

        

        $('body').on('click', '.delete_workPayment', function(){
            var id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('admin.workPayment.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                totalPaid();
                                showData();
                            }
                        }
                    });
                }
            });
        });

    
    });
    
</script>
@endpush