@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.workPayment.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> Add Work Payment</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createWorkPayment','class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.timeLogs.employeeName')</label>
                                            <select class="select2 form-control" data-placeholder="Choose Employee" id="user_id" name="user_id">
                                                @foreach($employees as $employee)
                                                    <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <label class="control-label">@lang('app.selectDateRange')</label>
                
                                        <div class="form-group">
                                            <input class="form-control input-daterange-datepicker" type="text" name="daterange"
                                                   value="{{ $startDate->format('m/d/Y').' - '.$endDate->format('m/d/Y') }}"/>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Payment</label>
                                            <input type="number" min="0" name="payment" id="payment" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>payment Date</label>
                                            <input type="text" name="pay_date" id="pay_date" value="" class="form-control pay_date">
                                        </div>
                                    </div>

                                   
                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>payable</label>
                                            <input type="number" min="0" name="payable" id="payable" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    

                                   
                                </div>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Bonus</label>
                                            <input type="number" min="0" name="bonus" id="bonus" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Balance</label>
                                            <input type="number" min="0" name="balance" id="balance" class="form-control">
                                        </div>
                                    </div>

                                   
                                </div>
                                <!--/row-->

                            

                            </div>
                            <div class="form-actions">
                                <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
    var startDate = '{{ $startDate->format('Y-m-d') }}';
    var endDate = '{{ $endDate->format('Y-m-d') }}';

   

        


    $("#pay_date").datepicker({
        todayHighlight: true,
        autoclose: true
    });

    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        cancelClass: 'btn-inverse',
        "locale": {
            "applyLabel": "{{ __('app.apply') }}",
            "cancelLabel": "{{ __('app.cancel') }}",
            "daysOfWeek": [
                "{{ __('app.su') }}",
                "{{ __('app.mo') }}",
                "{{ __('app.tu') }}",
                "{{ __('app.we') }}",
                "{{ __('app.th') }}",
                "{{ __('app.fr') }}",
                "{{ __('app.sa') }}"
            ],
            "monthNames": [
                "{{ __('app.january') }}",
                "{{ __('app.february') }}",
                "{{ __('app.march') }}",
                "{{ __('app.april') }}",
                "{{ __('app.may') }}",
                "{{ __('app.june') }}",
                "{{ __('app.july') }}",
                "{{ __('app.august') }}",
                "{{ __('app.september') }}",
                "{{ __('app.october') }}",
                "{{ __('app.november') }}",
                "{{ __('app.december') }}",
            ]
        }
    })

    $('.input-daterange-datepicker').on('apply.daterangepicker', function (ev, picker) {
        startDate = picker.startDate.format('YYYY-MM-DD');
        endDate = picker.endDate.format('YYYY-MM-DD');
        console.log(startDate);
        console.log(endDate);
        get_payable();
    });

    $("#payment").change(function(){
       var payment = parseInt($("#payment").val());
       var payable = parseInt($("#payable").val());
    console.log(payment);
    console.log(payable);
       if(payment > payable){
          
            var bonus = payment - payable;
            $("#bonus").val(bonus);
            $("#balance").val('');
       }
       else if(payable > payment){
            var balance = payable - payment;
            $("#balance").val(balance);
            $("#bonus").val('');
       }
    });

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.workPayment.store')}}',
            container: '#createWorkPayment',
            type: "POST",
            redirect: true,
            data: $('#createWorkPayment').serialize() + '&startDate=' + startDate + '&endDate=' + endDate,
            success: function (data) {
                if($.isEmptyObject(data.error)){
                    }else{
                        printErrorMsg(data.error);
                        $("#amount").css('border-color', 'red');
                        $("#from_date").css('border-color', 'red');

                    }
            }
        })
    });

    function printErrorMsg (msg) {
        
            $.each( msg, function( key, value ) {
                $(".amount_error").html('<div class="alert alert-error" style="color:red"><strong>'+ value +'</strong><div>');
            });
        }

    
    var userId = $('#user_id').val();
        if (userId == "") {
            userId = 0;
        }
        console.log(userId);

    $('#user_id').change(function () {
         userId = $("#user_id").val();
        get_payable();
    });


    function get_payable() {
        $('body').block({
            message: '<p style="margin:0;padding:8px;font-size:24px;">Just a moment...</p>'
            , css: {
                color: '#fff'
                , border: '1px solid #fb9678'
                , backgroundColor: '#fb9678'
            }
        });

        
        

        //refresh payable
        var url = '{!!  route('admin.workPayment.refreshPayable', [':startDate', ':endDate', ':userId']) !!}';
        url = url.replace(':startDate', startDate);
        url = url.replace(':endDate', endDate);
        url = url.replace(':userId', userId);
        

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                //totalHours = response.totalHours;
                $('#payable').val(response.amount_till_now);
                var payment = parseInt($("#payment").val());
                var payable = parseInt($("#payable").val());
                console.log(payment);
                console.log(payable);
                if(payment > payable){
                    
                        var bonus = payment - payable;
                        $("#bonus").val(bonus);
                        $("#balance").val('');
                }
                else if(payable > payment){
                        var balance = payable - payment;
                        $("#balance").val(balance);
                        $("#bonus").val('');
                        }
            }
        });

  
    }
    get_payable();
</script>
@endpush

