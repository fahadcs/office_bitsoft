@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }} #{{ $project->id }} - <span class="font-bold">{{ ucwords($project->project_name) }}</span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                @if($role_id == 1)
                <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ $pageTitle }}</a></li>
                @elseif($role_id == 2)
                <li><a href="{{ route('member.dashboard') }}">Home</a></li>
                <li><a href="{{ route('member.projects.index') }}">{{ $pageTitle }}</a></li>
                @else
                <li><a href="{{ route('member.dashboard') }}">Home</a></li>
                <li><a href="{{ route('member.projects.index') }}">{{ $pageTitle }}</a></li>
                @endif
                <li class="active">Credentials</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/icheck/skins/all.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">

                        <nav>
                            <ul>
                                @if($role_id == 1)
                                <li ><a href="{{ route('admin.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a>
                                </li>
                                <li class="tab-current"><a href="{{ route('notes', $project->id) }}"><span>Credentials</span></a>
                                @if(\App\ModuleSetting::checkModule('employees'))
                                <li><a href="{{ route('admin.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a></li>
                                @endif

                                <li><a href="{{ route('admin.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                <li><a href="{{ route('admin.files.show', $project->id) }}"><span>@lang('modules.projects.files')</span></a>
                                </li>

                                @if(\App\ModuleSetting::checkModule('invoices'))
                                <li><a href="{{ route('admin.invoices.show', $project->id) }}"><span>@lang('app.menu.invoices')</span></a></li>
                                @endif

                                @if(\App\ModuleSetting::checkModule('timelogs'))
                                <li><a href="{{ route('admin.time-logs.show', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                @endif
                                @elseif($role_id == 2)
                                    <li><a href="{{ route('member.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a></li>
                                    @if(\App\ModuleSetting::checkModule('employees'))
                                    <li class="tab-current"><a href="{{ route('notes', $project->id) }}"><span>Credentials</span></a>
                                    <li><a href="{{ route('member.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a></li>
                                    @endif

                                    @if(\App\ModuleSetting::checkModule('tasks'))
                                    <li><a href="{{ route('member.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                    @endif

                                    <li><a href="{{ route('member.files.show', $project->id) }}"><span>@lang('modules.projects.files')</span></a> </li>

                                    @if(\App\ModuleSetting::checkModule('timelogs'))
                                    <li><a href="{{ route('member.time-log.show-log', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                    @endif
                                @else
                                <li><a href="{{ route('member.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a></li>
                                    @if(\App\ModuleSetting::checkModule('employees'))
                                    <li><a href="{{ route('notes', $project->id) }}"><span>Credentials</span></a>
                                    <li><a href="{{ route('member.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a></li>
                                    @endif

                                    @if(\App\ModuleSetting::checkModule('tasks'))
                                    <li><a href="{{ route('member.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                    @endif

                                    <li class="tab-current"><a href="{{ route('member.files.show', $project->id) }}"><span>@lang('modules.projects.files')</span></a> </li>

                                    @if(\App\ModuleSetting::checkModule('timelogs'))
                                    <li><a href="{{ route('member.time-log.show-log', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                    @endif
                                @endif    

                                
                            </ul>
                        </nav>
                    </div>
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-md-12" id="task-list-panel">
                                    <div class="white-box">
                                        <h2>Credentials</h2>

                                        <div class="row m-b-10">
                                            <div class="col-md-5">
                                                <a href="javascript:;" id="show-new-task-panel"
                                                   class="btn btn-success btn-outline"><i class="fa fa-plus"></i> Add New Note</a>
                                            </div>
                                        </div>

                                        <div class="row m-b-10">
                                            <div class="col-md-5">
                                                <select class="sort-task-style form-control category_main" id="sort-note">
                                                    <option value="all">All</option>
                                                    @foreach($pNotesCategory as $category)
                                                        @if(count($category->getNotesById($project->id, $role_id)) > 0)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        </div>
                                        <ul class="list-group" id="notes_card">
                                            @foreach($pNotesCategory as $item)
                                                @if(count($item->getNotesById($project->id, $role_id)) > 0)
                                                <div class="col-md-6 categoryId-{{$item->id}}">
                                                    <li class="list-group-item" style="margin-bottom:10px;margin-top:10;">
                                                        <h1>{{$item->name}}</h1>
                                                             <ul class="list-group">
                                                                 @foreach($item->getNotesById($project->id, $role_id) as $data)
                                                                     @if($data != '')    
                                                                         <li style="list-style:none;">
                                                                             <div>
                                                                                 {!! $data->note !!}
                                                                             </div>
                                                                             @if($role_id == 1)
                                                                             <p><strong>Show To Member</strong> : @if($data->show_to_member == true) Yes @else No @endif</p>
                                                                             @endif
                                                                             <button class="btn btn-success edit-note" data-note_id="{{$data->id}}"><i
                                                                                    class="fa fa-check"></i> Edit
                                                                             </button>

                                                                         </li>
                                                                         <hr>
                                                                     @endif
                                                                 @endforeach
                                                             </ul>
                                                     </li>
                                                </div>
                                                @endif
                                            @endforeach

                                        </ul>
                                    
                                </div>

                                <div class="col-md-6 hide" id="new-task-panel">
                                    <div class="panel panel-default">
                                        <div class="panel-heading "><i class="ti-plus"></i> New Note
                                            <div class="panel-action">
                                                <a href="javascript:;" id="hide-new-task-panel"><i class="ti-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                {!! Form::open(['id'=>'createNote','class'=>'ajax-form','method'=>'POST']) !!}

                                                {!! Form::hidden('project_id', $project->id) !!}

                                                <div class="form-body">
                                                    <div class="row">
                                                        
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">@lang('app.description')</label>
                                                                <textarea id="description" name="description"
                                                                          class="form-control summernote"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-8">
                                                            <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                                            <div class="form-group">
                                                                <select class="form-control" name="category" id="category">
                                                                        @foreach($pNotesCategory as $category)
                                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                                        @endforeach
                                                                        
                                                                </select>
                                                            </div>
                                                        </div>
                                                        

                                                        <div class="col-md-2">
                                                            <label class="control-label">Add New</label>
                                                            <a href="javascript:;"
                                                                    class="btn btn-success btn-outline" data-toggle="modal" data-target="#addCategory"><i class="fa fa-plus"></i> Add New category</a>
                                                        </div>
                                                        @if($role_id == 1)
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Show To Member</label><br>
                                                                <input name="show_to_member" type="checkbox">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        
                                                    </div>
                                                    <!--/row-->

                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" id="save-note" class="btn btn-success"><i
                                                                class="fa fa-check"></i> @lang('app.save')
                                                    </button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 hide" id="edit-task-panel">
                                </div>
                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->


    <div class="modal fade" id="addCategory" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            
            <div class="modal-body">
                {!! Form::open(['id'=>'createCategory','class'=>'ajax-form','method'=>'POST']) !!}

                {!! Form::hidden('project_id', $project->id) !!}

                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">@lang('app.title')</label>
                                <input type="text" id="category_heading" name="category_heading"
                                       class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">@lang('app.description')</label>
                                <textarea id="category_description" name="category_description"
                                          class="form-control"></textarea>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                    <!--/row-->

                </div>
                <div class="form-actions">
                    <button type="submit" id="save-category" class="btn btn-success"><i
                                class="fa fa-check"></i> @lang('app.save')
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
            
          </div>
          
        </div>
      </div>

@endsection

@push('footer-script')
<script src="{{ asset('js/cbpFWTabs.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    var newTaskpanel = $('#new-task-panel');
    var taskListPanel = $('#task-list-panel');
    var editTaskPanel = $('#edit-task-panel');

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    //    save new task
    $('#save-note').click(function () {
        $.easyAjax({
            url: '{{route('notes.saveNote')}}',
            container: '#createNote',
            type: "POST",
            data: $('#createNote').serialize(),
            formReset: true,
            success: function (data) {
                newTaskpanel.addClass('hide').removeClass('show');
                taskListPanel.switchClass("col-md-6", "col-md-12", 1000, "easeInOutQuad");
                $("#notes_card").html(data.html);
                $(".category_main").html(data.category_options);
                
            }
        })
    });

    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('notes.saveCategory')}}',
            container: '#createCategory',
            type: "POST",
            data: $('#createCategory').serialize(),
            formReset: true,
            success: function (data) {
                var html;
                var category = data.category;
                
                    
                        html = '<option value="'+category['id']+'">'+category['name']+'</option>';
                        $("#category").append(html);
                        $("#category").val(category['id']);
                        $("#sort-note").append(html);
                        $("#sort-note").val(category['id']);
                        $(".category_dropdown").append(html);
                        $(".category_dropdown").val(category['id']);
                        

                        console.log(html);
                    
                    
                
                $("#addCategory").modal('hide');
            }
        })
    });

    //    save new task
    taskListPanel.on('click', '.edit-note', function () {
        var id = $(this).data('note_id');
        var url = "{{route('notes.editNote', ':id')}}";
        url = url.replace(':id', id);

        $.easyAjax({
            url: url,
            type: "GET",
            data: {noteId: id},
            success: function (data) {
                editTaskPanel.html(data.html);
                taskListPanel.switchClass("col-md-12", "col-md-6", 1000, "easeInOutQuad");
                newTaskpanel.addClass('hide').removeClass('show');
                editTaskPanel.switchClass("hide", "show", 300, "easeInOutQuad");
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            }
        })
    });

    //    change task status
    
    //    save new task
    $('#sort-note').change(function() {
        if(this.value == 'all'){
            $("#notes_card").show().children().show();
        }
        else{
            $(".categoryId-"+this.value).show();
            $(".categoryId-"+this.value).siblings().hide();
        }
        
        
    });

    $('#show-new-task-panel').click(function () {
//    taskListPanel.switchClass('col-md-12', 'col-md-8', 1000, 'easeInOutQuad');
        taskListPanel.switchClass("col-md-12", "col-md-6", 1000, "easeInOutQuad");
        editTaskPanel.addClass('hide').removeClass('show');
        newTaskpanel.switchClass("hide", "show", 300, "easeInOutQuad");
    });

    $('#hide-new-task-panel').click(function () {
        newTaskpanel.addClass('hide').removeClass('show');
        taskListPanel.switchClass("col-md-6", "col-md-12", 1000, "easeInOutQuad");
    });

    editTaskPanel.on('click', '#hide-edit-task-panel', function () {
        editTaskPanel.addClass('hide').removeClass('show');
        taskListPanel.switchClass("col-md-6", "col-md-12", 1000, "easeInOutQuad");
    });

    
    $('.summernote').summernote({
        height: 100,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false                 // set focus to editable area after initializing summernote
    });

</script>
@endpush