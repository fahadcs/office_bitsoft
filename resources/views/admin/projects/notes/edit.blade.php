<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">

<div class="panel panel-default">
    <div class="panel-heading "><i class="ti-pencil"></i> Update Note
        <div class="panel-action">
            <a href="javascript:;" class="close" id="hide-edit-task-panel" data-dismiss="modal"><i class="ti-close"></i></a>
        </div>
    </div>
    <div class="panel-wrapper collapse in">
        <div class="panel-body">
            {!! Form::open(['id'=>'updateNote','class'=>'ajax-form','method'=>'POST']) !!}
            {!! Form::hidden('project_id', $note->project_id) !!}
            {!! Form::hidden('id', $note->id) !!}

            <div class="form-body">
                <div class="row">
                    
                    <!--/span-->
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">@lang('app.description')</label>
                            <textarea id="description" name="description" class="form-control summernote">{{ $note->note }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                        <div class="form-group">
                            <select class="form-control category_dropdown" name="category" id="category">
                                    @foreach($pNotesCategory as $category)
                                        <option @if($note->p_note_category_id == $category->id) selected @endif value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                    
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Add New</label>
                        <a href="javascript:;"
                                class="btn btn-success btn-outline" data-toggle="modal" data-target="#addCategory"><i class="fa fa-plus"></i> Add New category</a>
                    </div>
                    @if($role_id == 1)
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Show To Member</label><br>
                            <input name="show_to_member" type="checkbox" @if($note->show_to_member == true) checked="checked" @endif>
                        </div>
                    </div>
                    @endif
                </div>
                <!--/row-->

            </div>
            <div class="form-actions">
                <button type="button" id="update-note" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script>
     var newTaskpanel = $('#new-task-panel');
    var taskListPanel = $('#task-list-panel');
    var editTaskPanel = $('#edit-task-panel');
   $('#update-note').click(function () {
        $.easyAjax({
            url: '{{route('notes.saveNote')}}',
            container: '#updateNote',
            type: "POST",
            data: $('#updateNote').serialize(),
            formReset: true,
            success: function (data) {
                editTaskPanel.addClass('hide').removeClass('show');
                taskListPanel.switchClass("col-md-6", "col-md-12", 1000, "easeInOutQuad");
                $("#notes_card").html(data.html);
                $("#sort-note").html(data.category_options);
            }
        })
    });
    $('.summernote').summernote({
        height: 100,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false                 // set focus to editable area after initializing summernote
    });
</script>
