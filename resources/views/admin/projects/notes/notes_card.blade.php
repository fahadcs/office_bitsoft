@foreach($category as $item)
                                                @if(count($item->getNotesById($project_id, $role_id)) > 0)
                                                <div class="col-md-6 categoryId-{{$item->id}}">
                                                    <li class="list-group-item" style="margin-bottom:10px;margin-top:10;">
                                                        <h1>{{$item->name}}</h1>
                                                             <ul class="list-group">
                                                                 @foreach($item->getNotesById($project_id, $role_id) as $data)
                                                                     @if($data != '')    
                                                                         <li style="list-style:none;">
                                                                             <div>
                                                                                 {!! $data->note !!}
                                                                             </div>
                                                                             @if($role_id == 1)
                                                                             <p><strong>Show To Member</strong> : @if($data->show_to_member == true) Yes @else No @endif</p>
                                                                             @endif
                                                                             <button class="btn btn-success edit-note" data-note_id="{{$data->id}}"><i
                                                                                    class="fa fa-check"></i> Edit
                                                                             </button>

                                                                         </li>
                                                                         <hr>
                                                                     @endif
                                                                 @endforeach
                                                             </ul>
                                                     </li>
                                                </div>
                                                @endif
@endforeach