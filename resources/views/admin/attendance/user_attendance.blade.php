<thead>
    <tr>
        <th>@lang('app.date')</th>
        <th>Attendance</th>
        
    </tr>
</thead>
    <tbody id="attendanceData">
 

@for ($date = $endDate; $date->diffInDays($startDate) > 0; $date->subDay())
    <?php
        $present = 0;
        $attendanceData = '';
        
        $present_user = [];
        $all_user = [];
        $user_data=[];
        
    ?>

    @foreach($attendances as $attendance)
        @if($attendance->clock_in_date == $date->toDateString())
            <?php
                $present = 1;
                $attendanceData = $attendance;
                array_push($present_user, $attendance);

            ?>
        @endif

        
        
    @endforeach
    

    <?php 
    
        foreach($employees as $user){
            $user_data['name'] = $user->name;
            $user_data['present'] = 0;
            $user_data['clock_in'] = '-';
            $user_data['clock_out'] = '-';
            $user_data['working_from'] = '-';
            $user_data['work_summary'] = '-';
            $user_data['deleted_id'] = 0;

            foreach($present_user as $data){
                if($user->name == $data->name){
                    $user_data['present'] = 1;
                    if(!is_null($data->clock_in_time)){
                        $user_data['clock_in'] = $data->clock_in_time->timezone($global->timezone)->format('h:i A');
                    }
                    else{
                        $user_data['clock_in'] = '-';
                    }
                    if(!is_null($data->clock_out_time)){
                        $user_data['clock_out'] = $data->clock_out_time->timezone($global->timezone)->format('h:i A');
                    }
                    else{
                        $user_data['clock_out'] = '-';
                    }
                    if($data->working_from != ''){
                        $user_data['working_from'] = $data->working_from;
                    }
                    if($data->work_summary != ''){
                        $user_data['work_summary'] = $data->work_summary;
                    }

                    $user_data['deleted_id'] = $data->aId;
                }
                else{
                    continue;
                }
            }
        

            array_push($all_user, $user_data);
        }

        ?>



    
        <tr class="@if($date->format('l') == 'Sunday')sunday @endif">
            <td>@lang('app.'.strtolower( $date->format("F") )) {{ $date->format('d, Y') }} <br><br> <label class="label label-success">{{ $date->format('l') }}</label>  @if(in_array($date->format('Y-m-d'), $leavesDate)) <label class="label label-danger">@lang('app.onLeave')</label> @endif</td>
            <td>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Clock in</th>
                            <th>Clock out</th>
                            <th>Working From</th>
                            <th width="200px">work summary</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($all_user as $user)
                            <tr @if($user['present'] == 1) class="status_present" @else class="status_absent" @endif>
                                <td>{{$user['name']}}</td>
                                <td>@if($user['present'] == 1)<label class="label label-success">@lang('modules.attendance.present')</label> @else <label class="label label-danger">@lang('modules.attendance.absent')</label> @endif</td>
                                <td>{{$user['clock_in']}}</td>
                                <td>{{$user['clock_out']}}</td>
                                <td>{{$user['working_from']}}</td>
                                <td>{{$user['work_summary']}}</td>
                                <td>@if($user['deleted_id'] == 0) - @else <a href="javascript:;" data-attendance-id="{{ $user['deleted_id'] }}" class="delete-attendance btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i> @lang('app.delete') </a> @endif</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
        </tr>
@endfor

<?php
        $present = 0;
        $attendanceData = '';
        $present_user = [];
        $all_user = [];
       
        $user_data=[];
?>

@foreach($attendances as $attendance)
    @if($attendance->clock_in_date == $date->toDateString())
        <?php
        $present = 1;
        $attendanceData = $attendance;
        array_push($present_user, $attendance);
        ?>
    @endif

    
@endforeach
<?php 
        foreach($employees as $user){
            $user_data['name'] = $user->name;
            $user_data['present'] = 0;
            $user_data['clock_in'] = '-';
            $user_data['clock_out'] = '-';
            $user_data['working_from'] = '-';
            $user_data['work_summary'] = '-';
            $user_data['deleted_id'] = 0;

            foreach($present_user as $data){
                if($user->name == $data->name){
                    $user_data['present'] = 1;
                    if(!is_null($data->clock_in_time)){
                        $user_data['clock_in'] = $data->clock_in_time->timezone($global->timezone)->format('h:i A');
                    }
                    else{
                        $user_data['clock_in'] = '-';
                    }
                    if(!is_null($data->clock_out_time)){
                        $user_data['clock_out'] = $data->clock_out_time->timezone($global->timezone)->format('h:i A');
                    }
                    else{
                        $user_data['clock_out'] = '-';
                    }
                    if($data->working_from != ''){
                        $user_data['working_from'] = $data->working_from;
                    }
                    if($data->work_summary != ''){
                        $user_data['work_summary'] = $data->work_summary;
                    }

                    $user_data['deleted_id'] = $data->aId;
                }
                else{
                    continue;
                }
            }
        

            array_push($all_user, $user_data);
        }

        ?>

    <tr class="@if($date->format('l') == 'Sunday')sunday @endif">
        <td>@lang('app.'.strtolower( $date->format("F") )) {{ $date->format('d, Y') }}</td>
        <td>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Clock in</th>
                        <th>Clock out</th>
                        <th>Working From</th>
                        <th width="200px">work Summary</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($all_user as $user)
                        <tr @if($user['present'] == 1) class="status_present" @else class="status_absent" @endif>
                            <td>{{$user['name']}}</td>
                            <td>@if($user['present'] == 1)<label class="label label-success">@lang('modules.attendance.present')</label> @else <label class="label label-danger">@lang('modules.attendance.absent')</label> @endif</td>
                            <td>{{$user['clock_in']}}</td>
                            <td>{{$user['clock_out']}}</td>
                            <td>{{$user['working_from']}}</td>
                            <td>{{$user['work_summary']}}</td>
                            <td>@if($user['deleted_id'] == 0) - @else <a href="javascript:;" data-attendance-id="{{ $user['deleted_id'] }}" class="delete-attendance btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i> @lang('app.delete')</a> @endif</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </td>
    </tr>

</tbody>
<style>

    .sunday{
        display: none;
    }
</style>