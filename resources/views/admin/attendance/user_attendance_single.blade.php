<thead>
    <tr>
        <th>@lang('app.date')</th>
        <th>@lang('app.status')</th>
        <th>@lang('modules.attendance.clock_in')</th>
        <th>@lang('modules.attendance.clock_out')</th>
        <th width="200px">@lang('app.others')</th>
    </tr>
    </thead>
    <tbody>
        @for ($date = $endDate; $date->diffInDays($startDate) > 0; $date->subDay())
    <?php
        $present = 0;
        $attendanceData = '';
       
        
    ?>

    @foreach($attendances as $attendance)
        @if($attendance->clock_in_date == $date->toDateString())
            <?php
                $present = 1;
                $attendanceData = $attendance;
                

            ?>
        @endif
    @endforeach
    
    @if($present == 1)
    
        <tr class="status_present @if($date->format('l') == 'Sunday')sunday @endif">
            <td>@lang('app.'.strtolower( $date->format("F") )) {{ $date->format('d, Y') }} <br><br> <label class="label label-success">{{ $date->format('l') }}</label>  @if(in_array($date->format('Y-m-d'), $leavesDate)) <label class="label label-danger">@lang('app.onLeave')</label> @endif</td>
            <td><label class="label label-success">@lang('modules.attendance.present')</label></td>
            <td>{{ $attendanceData->clock_in_time->timezone($global->timezone)->format('h:i A') }}</td>
            <td>@if(!is_null($attendanceData->clock_out_time)) {{ $attendanceData->clock_out_time->timezone($global->timezone)->format('h:i A') }} @endif</td>
            <td>
                
                <strong>@lang('modules.attendance.working_from'): </strong> {{ $attendanceData->working_from }}<br>
                <strong>Work Summary: </strong>
                {{$attendanceData->work_summary}}
                <a href="javascript:;" data-attendance-id="{{ $attendanceData->aId }}" class="delete-attendance btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i> @lang('app.delete')</a>
                
            </td>
        </tr>
    @else
        <tr class="status_absent @if($date->format('l') == 'Sunday')sunday @endif">
            <td>@lang('app.'.strtolower( $date->format("F") )) {{ $date->format('d, Y') }} <br><br> <label class="label label-success">{{ $date->format('l') }}</label>@if(in_array($date->format('Y-m-d'), $leavesDate)) <label class="label label-danger">@lang('app.onLeave')</label> @endif </td>
            <td><label class="label label-danger">@lang('modules.attendance.absent')</label></td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
        </tr>
    @endif

@endfor

<?php
$present = 0;
$attendanceData = '';
$date = $endDate;

?>

@foreach($attendances as $attendance)
    @if($attendance->clock_in_date == $date->toDateString())
        <?php
        $present = 1;
        $attendanceData = $attendance;
        
        ?>
    @endif
@endforeach
@if($present == 1)
    <tr class="status_present @if($date->format('l') == 'Sunday')sunday @endif">
        <td>@lang('app.'.strtolower( $date->format("F") )) {{ $date->format('d, Y') }}</td>
        <td><label class="label label-success">@lang('modules.attendance.present')</label></td>
        <td>{{ $attendanceData->clock_in_time->timezone($global->timezone)->format('h:i A') }}</td>
        <td>@if(!is_null($attendanceData->clock_out_time)) {{ $attendanceData->clock_out_time->timezone($global->timezone)->format('h:i A') }} @endif</td>
        <td>
           
            <strong>@lang('modules.attendance.working_from'): </strong> {{ $attendanceData->working_from }}<br>
            <strong>Work Summary: </strong>
            {{$attendanceData->work_summary}}
            <a href="javascript:;" data-attendance-id="{{ $attendanceData->aId }}" class="delete-attendance btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i> @lang('app.delete')</a>
             
        </td>
    </tr>
@else
    <tr class="status_absent @if($date->format('l') == 'Sunday')sunday @endif">
        <td>@lang('app.'.strtolower( $date->format("F") )) {{ $date->format('d, Y') }}</td>
        <td><label class="label label-danger">@lang('modules.attendance.absent')</label></td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
    </tr>
@endif
</tbody>

<style>

    .sunday{
        display: none;
    }
</style>

