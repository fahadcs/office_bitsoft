<li class="top-notifications">
    <div class="message-center">
        <a href="javascript:;" class="show-all-notifications">
            <div class="user-img">
                <span class="btn btn-circle btn-info"><i class="icon-list"></i></span>
            </div>
            @if(isset($notification->data['heading']) && isset($notification->data['created_at']))
            <div class="mail-contnet">
                <span class="mail-desc m-0">New task for you - {{ ucfirst($notification->data['heading']) }}</span> <span class="time">{{ $notification->data['created_at'] }}</span>
            </div>
            @endif
        </a>
    </div>
</li>